# crateriform
Визуализатор работы интерпретатор Lua, который я заслужил.

Содержит write-only кода более, чем наполовину.

## Установка
```
$ (identify -version && \
   tar --version && \
   python3 -V && \
   python3 -m venv -h && \
   lua5.3 -v && \
   awk -V && \
   ffmpeg -version && \
   make -v) 2>&1 > /dev/null && echo 'all set'
```

Если команда выше пишет что угодно, но не `all set`, доставьте отсутствующее:

* imagemagick
* GNU tar
* python 3.6+
* python3-venv
* lua 5.3
* GNU awk
* ffmpeg
* GNU make

```
# luarocks install 30log inspect
$ git clone https://gitlab.com/cc-ru/crateriform.git
$ cd crateriform
$ python3 -m venv python/env
$ python/env/bin/pip install numpy pygments pygame
```

## Запуск
```
$ make debug
```

Если команда выше работает исправно и без ошибок, то можно запускать:

```
$ make
```

Когда закончит, появятся файлы `*/render.mp4` — по одному для каждого примера.

## Примеры
Сами примеры лежат в `/programs`.

## Благодарности
- Спасибо мне за стэктрейсилку.
- Спасибо @LeshaInc за охрененную либу — парсер Lua.
- И @MeXaN1cK, который на своих мощностях более десятка раз всё перерендеривал.

## Зачем?
Это всё отрендерило мне кучу видяшек для моего гайда по корутинам в Lua.
[Рекомендую почитать](https://fingercomp.gitlab.io/lua-coroutines/).
