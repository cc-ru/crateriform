local co = coroutine.create(function() end)
print(coroutine.resume(co))
--> true

print(coroutine.status(co))
--> dead

print(coroutine.resume(co))
--> false   cannot resume dead coroutine
