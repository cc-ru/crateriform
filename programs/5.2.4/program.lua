local tree = {content = "root",
              children = {{content = "a",
                           children = {{content = "b", children = {}},
                                       {content = "c", children = {}}}},
                          {content = "d",
                           children = {{content = "e",
                                        children = {{content = "f",
                                                     children = {}}}},
                                       {content = "g",
                                        children = {{content = "h",
                                                     children = {}}}}}}}}

local function dfsPreOrder(node, f)
  if f(node) then
    return true
  end

  for _, child in ipairs(node.children) do
    if dfsPreOrder(child, f) then
      return true
    end
  end
end

local count = 0

dfsPreOrder(tree, function(node)
  print(node.content)
  count = count + 1

  return count > 30
end)
