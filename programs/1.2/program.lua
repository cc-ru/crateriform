local function fibonacciSeq(n)
  local penultimate = 1
  local i = 0

  return function(_, previous)
    local result = penultimate + previous
    penultimate = previous
    i = i + 1

    if i <= n then
      return result
    end
  end, nil, 0
end

for n in fibonacciSeq(10) do
  print(n)
end
