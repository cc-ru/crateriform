local function natural(start)
  start = start or 1

  return coroutine.wrap(function()
    for i = start, math.huge, 1 do
      coroutine.yield(i)
    end
  end)
end
