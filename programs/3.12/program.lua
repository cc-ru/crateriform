local co = coroutine.create(function()
  coroutine.yield()
end)

print(coroutine.status(co))
--> suspended

coroutine.resume(co)
print(coroutine.status(co))
--> suspended
