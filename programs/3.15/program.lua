local co = coroutine.create(function()
  error("error example")
end)

coroutine.resume(co)
print(coroutine.status(co))
--> dead
