local mainThread = coroutine.running()

local function transfer(co, ...)
  if coroutine.running() ~= mainThread then
    return coroutine.yield(co, ...)
  end

  local data = table.pack(...)

  while true do
    data = table.pack(
      coroutine.resume(
        co,
        table.unpack(data,
                     1,
                     data.n)
      )
    )

    local success = table.remove(data, 1)

    if not success then
      error(data[1])
    end

    data.n = data.n - 1

    if coroutine.status(co) == "dead" then
      return table.unpack(data, 1, data.n)
    end

    co = table.remove(data, 1)
    data.n = data.n - 1

    if co == mainThread then
      return table.unpack(data, 1, data.n)
    end
  end
end

local writer, reader

writer = coroutine.create(function(f)
  while true do
    local line = transfer(reader)

    if line then
      f:write(line)
    else
      break
    end
  end

  transfer(mainThread, f)
end)

reader = coroutine.create(function()
  for line in io.lines("sym.lua", "L") do
    transfer(writer, line)
  end

  transfer(writer, nil)
end)

local f = transfer(
  writer,
  io.open("sym-copy.lua", "w")
)

f:close()
