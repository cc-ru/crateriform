function coroutine.wrap(f)
  local co = coroutine.create(f)

  return function(...)
    local executionResult = table.pack(coroutine.resume(co, ...))

    if executionResult[1] then
      return table.unpack(executionResult, 2, executionResult.n)
    else
      error(executionResult[2], 2)
    end
  end
end
