local co = coroutine.create(function()
  return 7, 21, 42
end)

print(coroutine.resume(co))
--> true    7       21      42
