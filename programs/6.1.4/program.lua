local function natural(start)
  start = start or 1

  return coroutine.wrap(function()
    for i = start, math.huge, 1 do
      coroutine.yield(i)
    end
  end)
end

for i in natural() do
  print(i)

  if i >= 50 then
    break
  end
end
--> 1
--> 2
--> 3
--...it goes on and on...
--> 49
--> 50
