local f = coroutine.wrap(function(a)
  local b = coroutine.yield(a * 2)
  return a + b * 2
end)

print(f(3))
--> 6
print(f(6))
--> 15
