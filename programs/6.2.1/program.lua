local queue = {{type = "relay", responseUrl = "blah", url = "foo"}}

local function waitForEvent()
  if #queue == 0 then
    os.exit()
  end

  return table.remove(queue, 1)
end

local function sendRequest(url)
  table.insert(queue, {type = "http-connected", body = "ok"})
end

local handlers = {}

local function _add(eventType, handler, default)
  handlers[eventType] = handlers[eventType] or {}
  handlers[eventType][handler] = default
end

local function _del(eventType, handler)
  handlers[eventType][handler] = nil
end

local function addHandler(eventType, f)
  local handler = coroutine.create(f)
  _add(eventType, handler, eventType)
end

addHandler("relay", function(event)
  while true do
    local responseUrl = event.responseUrl
    sendRequest(event.url)
    sendRequest(responseUrl, coroutine.yield("http-connected").body)

    event = coroutine.yield()
  end
end)

while true do
  local event = waitForEvent()

  if handlers[event.type] then
    for handler, defaultEvent in pairs(handlers[event.type]) do
      local nextEvent = select(
        2,
        assert(
          coroutine.resume(handler,
                           event)
        )
      )

      if coroutine.status(handler) == "dead" then
        _del(event.type, handler)
      elseif not nextEvent and defaultEvent ~= event.type then
        _del(event.type, handler)
        _add(defaultEvent, handler, defaultEvent)
      elseif nextEvent and nextEvent ~= event.type then
        _del(event.type, handler)
        _add(nextEvent, handler, defaultEvent)
      end
    end
  end
end
