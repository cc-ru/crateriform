local f do
  local beforeYield = true
  local a, b

  f = function(val)
    if beforeYield then
      a = val
      beforeYield = false
      return a * 2
    else
      b = val
      return a + b * 2
    end
  end
end

print(f(3))
--> 6
print(f(6))
--> 15
