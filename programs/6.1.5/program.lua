local function unpack(t)
  return table.unpack(t, 1, t.n)
end

local function zip(...)
  local iterators = {...}

  return coroutine.wrap(function()
    while true do
      local merged = {n = 0}

      for _, iterator in ipairs(iterators) do
        local values = table.pack(iterator())

        if not values[1] then
          goto exhausted
        end

        table.move(values, 1, values.n, merged.n + 1, merged)
        merged.n = merged.n + values.n
      end

      coroutine.yield(unpack(merged))
    end

    ::exhausted::
  end)
end

local function iter(f, state, var)
  return coroutine.wrap(function()
    while true do
      local values = table.pack(f(state, var))
      var = values[1]

      if var == nil then
        break
      end

      coroutine.yield(unpack(values))
    end
  end)
end

local function natural(start)
  start = start or 1

  return coroutine.wrap(function()
    for i = start, math.huge, 1 do
      coroutine.yield(i)
    end
  end)
end

local iter = zip(
  natural(),
  iter(
    pairs({foo = "bar",
           bar = "baz",
           baz = "spam",
           spam = "eggs"})
  )
)

for i, k, v in iter do
  print(i, k, v)
end
--> 1       bar     baz
--> 2       foo     bar
--> 3       spam    eggs
--> 4       baz     spam
