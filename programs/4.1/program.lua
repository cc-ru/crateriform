local function inner(x)
  return coroutine.yield(x)
end

local co = coroutine.wrap(function(x)
  return 2 * inner(x)
end)

print(co(3))
--> 3

print(co(21))
--> 42
