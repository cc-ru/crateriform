local co = coroutine.create(function()
  coroutine.yield(42, 21, 7)
end)

print(coroutine.resume(co))
--> true    42      21      7
