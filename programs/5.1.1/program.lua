local function transfer(co, ...)
  if coroutine.running() ~= mainThread then
    return coroutine.yield(co, ...)
  end

  local data = table.pack(...)

  while true do
    data = table.pack(coroutine.resume(co, table.unpack(data, 1, data.n)))
    local success = table.remove(data, 1)

    if not success then
      error(data[1])
    end

    data.n = data.n - 1

    if coroutine.status(co) == "dead" then
      return table.unpack(data, 1, data.n)
    end

    co = table.remove(data, 1)
    data.n = data.n - 1

    if co == mainThread then
      return table.unpack(data, 1, data.n)
    end
  end
end
