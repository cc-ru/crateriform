local function executeRead(cmd)
  local proc = io.popen(cmd, "r")

  return coroutine.wrap(function()
    for line in proc:lines("L") do
      coroutine.yield(line)
    end

    proc:close()
  end)
end

local function head(n, producer)
  return coroutine.wrap(function()
    for i = 1, n do
      local line = producer()

      if not line then
        break
      end

      coroutine.yield(line)
    end
  end)
end

local function executeWrite(cmd, producer)
  local proc = io.popen(cmd, "w")

  for data in producer do
    assert(proc:write(data))
  end

  proc:close()
end

executeWrite(
  "gzip > test.gz",
  head(
    512,
    executeRead("find lua")
  )
)
