local tree = {content = "root",
              children = {{content = "a",
                           children = {{content = "b", children = {}},
                                       {content = "c", children = {}}}},
                          {content = "d",
                           children = {{content = "e",
                                        children = {{content = "f",
                                                     children = {}}}},
                                       {content = "g",
                                        children = {{content = "h",
                                                     children = {}}}}}}}}

local dfsPreOrder do
  local function traverse(node)
    coroutine.yield(node)

    for _, child in ipairs(node.children) do
      traverse(child)
    end
  end

  dfsPreOrder = function(node)
    return coroutine.wrap(traverse), node
  end
end

local count = 0

for node in dfsPreOrder(tree) do
  print(node.content)
  count = count + 1

  if count > 30 then
    break
  end
end
