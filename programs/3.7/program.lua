print(coroutine.resume(coroutine.create(function()
  return pairs(setmetatable({}, {__pairs = function()
    return coroutine.yield()
  end}))
end)))
--> false   attempt to yield across a C-call boundary
