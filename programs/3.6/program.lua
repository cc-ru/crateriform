coroutine.yield()
--! attempt to yield from outside a coroutine
--! stack traceback:
--!        [C]: in function 'coroutine.yield'
--!        example.lua:1: in main chunk
--!        [C]: in ?
