local co = coroutine.create(function(a)
  print("Got value a = " .. a)
  local b = coroutine.yield()
  print("Got value b = " .. b)

  return a + b
end)

coroutine.resume(co, 2)
--> Got value a = 2
coroutine.resume(co, 8)
--> Got value b = 8
