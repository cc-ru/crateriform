local co = coroutine.create(function()
  error("error example")
end)

print(coroutine.resume(co))
--> false   example.lua:2: error example

print(coroutine.resume(co))
--> false   cannot resume dead coroutine
