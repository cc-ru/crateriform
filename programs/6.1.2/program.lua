local function unpack(t)
  return table.unpack(t, 1, t.n)
end

local function filter(f, iterator)
  return coroutine.wrap(function()
    while true do
      local values = table.pack(iterator())

      if values[1] == nil then
        break
      end

      if f(unpack(values)) then
        coroutine.yield(unpack(values))
      end
    end
  end)
end

local function map(f, iterator)
  return coroutine.wrap(function()
    while true do
      local values = table.pack(iterator())

      if values[1] == nil then
        break
      end

      coroutine.yield(f(unpack(values)))
    end
  end)
end

local function takeFirst(n, iterator)
  return coroutine.wrap(function()
    for i = 1, n do
      local values = table.pack(iterator())

      if values[1] == nil then
        break
      end

      coroutine.yield(unpack(values))
    end
  end)
end

local function zip(...)
  local iterators = {...}

  return coroutine.wrap(function()
    while true do
      local merged = {n = 0}

      for _, iterator in ipairs(iterators) do
        local values = table.pack(iterator())

        if not values[1] then
          goto exhausted
        end

        table.move(values, 1, values.n, merged.n + 1, merged)
        merged.n = merged.n + values.n
      end

      coroutine.yield(unpack(merged))
    end

    ::exhausted::
  end)
end

local function chain(...)
  local iterators = {...}

  return coroutine.wrap(function()
    for _, iterator in ipairs(iterators) do
      while true do
        local values = table.pack(iterator())

        if values[1] == nil then
          break
        end

        coroutine.yield(unpack(values))
      end
    end
  end)
end

local function only(nth, iterator)
  return coroutine.wrap(function()
    while true do
      local value = select(nth, iterator())

      if value == nil then
        break
      end

      coroutine.yield(value)
    end
  end)
end

local function iter(f, state, var)
  return coroutine.wrap(function()
    while true do
      local values = table.pack(f(state, var))
      var = values[1]

      if var == nil then
        break
      end

      coroutine.yield(unpack(values))
    end
  end)
end


print("filter")
local function even(_, v)
  return v % 2 == 0
end

for k, v in filter(even, iter(ipairs({1, 2, 3, 4, 5}))) do
  print(k, v)
end
--> 2 2
--> 4 4

print("map")
for v in map(math.sqrt, only(2, iter(ipairs({1, 9, 25, 81})))) do
  print(v)
end
--> 1
--> 3
--> 5
--> 9

print("takeFirst")
for k, v in takeFirst(3, iter(ipairs({1, 2, 3, 4, 5}))) do
  print(k, v)
end
--> 1 1
--> 2 2
--> 3 3

print("zip")
for k1, v1, k2, v2 in zip(iter(ipairs({1, 2})), iter(ipairs({3, 4, 5}))) do
  print(k1, v1, k2, v2)
end
--> 1 1 1 3
--> 2 2 2 4

print("chain")
for k, v in chain(iter(ipairs({1, 2})), iter(ipairs({5, 4, 3}))) do
  print(k, v)
end
--> 1 1
--> 2 2
--> 3 5
--> 4 4
--> 5 3

print("only")
for v in only(2, iter(pairs({42, 21, 7}))) do
  print(v)
end
--> 42
--> 21
--> 7
