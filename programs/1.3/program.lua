local function fibonacciSeq(n)
  return coroutine.wrap(function()
    local penultimate, previous = 1, 0

    for i = 1, n do
      penultimate, previous = previous, penultimate + previous
      coroutine.yield(previous)
    end
  end)
end

for n in fibonacciSeq(10) do
  print(n)
end
