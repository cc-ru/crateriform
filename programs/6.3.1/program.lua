local function expectResponse(code, response)
  print(code, response)
end

local function establishConnection(addr)
  return {
    address = addr,
    connected = true,
    lines = addr == "1" and {
      "220",
      "331",
      "230",
      "test",
      "150",
    } or {
      "test",
      "test",
    },

    setBlocking = function(self, blocking)
      self.blocking = blocking
    end,

    isConnected = function(self)
      return self.connected
    end,

    readLine = function(self)
      return table.remove(self.lines, 1)
    end,

    send = function(self, data)
      print((('> "%s"'):format(data)
                       :gsub("\r", "\\r")
                       :gsub("\n", "\\n")))
    end,

    close = function(self)
      self.connected = false
    end
  }
end

local function send(data)
  return coroutine.yield("send", data)
end

local function pasv()
  local response = send("PASV\r\n")
  expectResponse(227, response)
  -- ...
  local dataAddr = response
  return dataAddr
end

local function newConnection(address, co)
  local socket = establishConnection(address)
  socket:setBlocking(true)

  return function()
    while socket:isConnected() do
      local response = socket:readLine()
      local tag, data = select(
        2,
        assert(
          coroutine.resume(
            co,
            response
          )
        )
      )

      if tag == "send" then
        socket:send(data)
      end

      if coroutine.status(co) == "dead" then
        socket:close()
      end
    end
  end
end

local function getFile(addr, user, pass, filename)
  return newConnection(addr, coroutine.create(function(response)
    expectResponse(220, response)
    expectResponse(331, send("USER " .. user .. "\r\n"))
    expectResponse(230, send("PASS " .. pass .. "\r\n"))
    local dataAddr = pasv()
    local f = assert(io.open("./" .. filename, "w"))

    local dataSocket = newConnection(
      dataAddr,
      coroutine.create(function(chunk)
        while chunk do
          f:write(chunk)
          chunk = coroutine.yield()
        end

        f:close()
      end)
    )

    expectResponse(150, send("RETR " .. filename .. "\r\n"))
    dataSocket()
    send("QUIT\r\n")
  end))()
end

getFile("1", "user", "pass", "./test.txt")
