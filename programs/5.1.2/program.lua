local function createCoroutine(f)
  return coroutine.create(function()
    f()
    error("coroutine returned")
  end)
end
