local mainThread = coroutine.running()

coroutine.resume(coroutine.create(function()
  print(coroutine.status(mainThread))
end))
--> normal
