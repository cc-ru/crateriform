local function fibonacciSeq(n)
  local result = {}

  for i = 1, n do
    result[i] = (result[i - 2] or 0) + (result[i - 1] or 1)
  end

  return ipairs(result)
end

for _, n in fibonacciSeq(10) do
  print(n)
end
