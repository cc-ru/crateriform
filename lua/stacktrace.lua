local i = require("inspect")

local function insertNonNil(t, v)
  if v then
    v = tostring(v)
    if #v > 0 then
      table.insert(t, v)
    end
  end
end

local function traceback(c)
  local message

  if type(c) == "string" then
    message = "error: " .. c .. "\n"
    c = nil
  end

  c = c or coroutine.running()

  for level = 0, math.huge do
    local info = debug.getinfo(c, level, "ufnSl")

    if not info then
      break
    end

    local funcType, name, args, exec, defined = {}, nil, {}, "", {}

    insertNonNil(funcType, info.what)
    insertNonNil(funcType, info.namewhat)
    table.insert(funcType, "function")

    name = info.name or "<anon>"

    if info.nparams then
      for an = 1, info.nparams do
        local argName, argValue = debug.getlocal(c, level, an)

        if argValue ~= nil then
          argName = argName .. "=" .. i(argValue)
        end

        table.insert(args, argName)
      end
    end

    if info.isvararg then
      table.insert(args, "...")
    end

    if info.currentline and info.currentline ~= -1 then
      exec = ":" .. tostring(info.currentline)
    end

    insertNonNil(defined, info.short_src)
    
    if info.linedefined and info.linedefined ~= -1 then
      table.insert(defined, info.linedefined)
    end

    funcType = table.concat(funcType, " ")
    args = table.concat(args, ", ")
    defined = (defined[1] and (" in " .. defined[1] .. "") or "") ..
              (defined[2] and (" at L" .. defined[2]) or "")

    -- local Lua function <anon>(a, b, c, ...):33 (defined in blah.lua at 33)

    local line = ("#%2d: %s %s%s%s%s"):format(
      level,
      funcType,
      name,
      #args > 0 and ("(" .. args .. ")") or "",
      exec,
      #defined > 0 and (" (defined" .. defined .. ")") or ""
    )

    if message then
      message = message .. line .. "\n"
    else
      print(line)
    end
  end

  return message
end

return traceback
