-- crateriform
-- Copyright (C) 2019 Fingercomp
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

unpack = nil
table.maxn = nil

local luaparse = require("luaparse")
local i = require("inspect")
local traceback = require("stacktrace")

local args = table.pack(...)

local lexer = luaparse.Lexer(io.open(args[1]):read("a"), args[1])
local parser = luaparse.Parser(lexer)

local block, message, slice = parser:block()

if not block then
  print("syntax error: " .. message)
  lexer:showSlice(slice)
  return
end

local stringq = function(s)
  return s:gsub("\\", "\\\\"):gsub("\n", "\\n"):gsub("\r", "\\r"):gsub('"', '\\"')
    :gsub("\t", "\\t"):gsub("\v", "\\v"):gsub("\f","\\f"):gsub("\a", "\\a")
    :gsub("\b", "\\b"):gsub("%c", function(c) return "\\" .. c:byte() end)
end

local compile = function(node)
  local h, hs, heval, hreset, hcall, hreturn, registerFunc, hshowCall
  local hexprList

  local showNode
  showNode = function(node, isStmt)
    if node.ty == "name" then
      return node.data
    elseif node.ty == "number" then
      return tostring(node.data)
    elseif node.ty == "nil" then
      return "nil"
    elseif node.ty == "boolean" then
      return tostring(node.data)
    elseif node.ty == "string" then
      return ("('%s')"):format(stringq(node.data))
    elseif node.ty == "dots" then
      return "..."
    elseif node.ty == "binop" then
      return "(" .. h(node.data.lhs) .. " " .. node.data.op .. " "
          .. h(node.data.rhs) .. ")"
    elseif node.ty == "unop" then
      return node.data.op .. " " .. h(node.data.node)
    elseif node.ty == "index" then
      if node.data.dot then
        return h(node.data.indexing) .. "." .. showNode(node.data.index)
      else
        return h(node.data.indexing) .. "[" .. h(node.data.index) .. "]"
      end
    elseif node.ty == "call" then
      local args = "(" .. hexprList(node.data.args, true) .. ")"

      local c = hcall(h(node.data.callable),
                      nil,
                      args,
                      node)

      if isStmt then
        c = ";" .. hs(c, node) .. ";"
      end

      return c
    elseif node.ty == "methodcall" then
      local args = "(" .. hexprList(node.data.args, true) .. ")"

      local c = hcall(h(node.data.callable),
                      showNode(node.data.method),
                      args,
                      node)

      if isStmt then
        c = ";" .. hs(c, node) .. ";"
      end

      return c
    elseif node.ty == "table" then
      local ret = "{"

      for k, v in pairs(node.data.map) do
        ret = ret .. "[" .. h(k) .. "] = " .. h(v) .. ", "
      end

      ret = ret .. hexprList(node.data.seq, node.data.last and true)

      return ret .. "}"
    elseif node.ty == "emptystat" then
      return ";"
    elseif node.ty == "do" then
      local ret = "do"

      for i, stat in ipairs(node.data) do
        ret = ret .. "\n" .. hs(stat) .. ";"
      end

      return ret .. "\nend"
    elseif node.ty == "if" then
      local ret = "if " .. hs(h(node.data.cond), node.data.condSlice, true) ..
                  " then"

      for i, stat in ipairs(node.data.block) do
        ret = ret .. "\n" .. showNode(stat, true) .. ";"
      end

      for i, branch in ipairs(node.data.pelseif) do
        ret = ret .. "\nelseif " .. hs(h(branch.cond), branch.slice, true) ..
                     " then"

        for i, stat in ipairs(branch.block) do
          ret = ret .. "\n" .. showNode(stat, true) .. ";"
        end
      end

      if node.data.pelse then
        ret = ret .. "\nelse\n" .. hs("", node.data.elseSlice) .. ";"

        for i, stat in ipairs(node.data.pelse) do
          ret = ret .. "\n" .. showNode(stat, true) .. ";"
        end
      end

      return ret .. "\nend"
    elseif node.ty == "while" then
      local ret = "while " .. hs(h(node.data.cond), node.data.topSlice, true) ..
                  " do"

      for i, stat in ipairs(node.data.block) do
        ret = ret .. "\n" .. showNode(stat, true) .. ";"
      end

      ret = ret .. hreset(node.slice)

      return ret .. "\nend"
    elseif node.ty == "repeat" then
      local block = ""

      for i, stat in ipairs(node.data.block) do
        block = block .. "\n" .. showNode(stat, true) .. ";"
      end

      return ([[
do
  local __firstIteration = true

  repeat
    if not __firstIteration then
      %s -- hreset
    else
      __firstIteration = nil
    end

    %s -- block
  until %s -- cond
end
]]):format(hreset(node.slice),
           block,
           hs(h(node.data.cond), node.data.bottomSlice, true))
    elseif node.ty == "goto" then
      return hs("goto " .. showNode(node.data), node)
    elseif node.ty == "label" then
      return hs("::" .. showNode(node.data) .. "::", node)
    elseif node.ty == "break" then
      return hs("break", node)
    elseif node.ty == "return" then
      local ret = hexprList(node.data, true)

      return hs("return " .. hreturn(ret, node), node)
    elseif node.ty == "for" then
      local block = ""

      for i, stat in ipairs(node.data.block) do
        block = block .. "\n" .. showNode(stat, true) .. ";"
      end

      local blockSlice = {node.data.block[1].slice[1],
                          node.data.block[1].slice[2],
                          node.data.block[#node.data.block].slice[3],
                          node.data.block[#node.data.block].slice[4]}

      if node.data.counter then
        return hs(([[
do
  local __firstIteration = true
  for %s = %s, %s%s do
    if not __firstIteration then
      %s -- reset
      %s -- eval counter
    else
      __firstIteration = nil
    end

    -- block
    %s
  end
end
]]):format(showNode(node.data.counter),
           h(node.data.init), h(node.data.limit), (node.data.step and
                                                   ", " .. h(node.data.step) or
                                                   ""),
           #node.data.block > 0 and hreset(blockSlice) or "",
           hs(heval(showNode(node.data.counter), node.data.init.slice),
              node.data.topSlice),
           block), node.data.topSlice)
      else
        local evalNames, names = "", ""

        for i, name in ipairs(node.data.names) do
          if i > 1 then
            names = names .. ", "
            evalNames = evalNames .. "\n "
          end

          names = names .. showNode(name)
          evalNames = evalNames .. heval(showNode(name), name.slice)
        end

        local exprs = hexprList(node.data.exps, 3)

        local exprSlice = {node.data.exps[1].slice[1],
                           node.data.exps[1].slice[2],
                           node.data.exps[#node.data.exps].slice[3],
                           node.data.exps[#node.data.exps].slice[4]}

        return hs(([[
do
  local __f, __s, __var = %s -- eval explist
  %s -- show as function call
  local __firstIteration = true

  while true do
    if not __firstIteration then
      %s -- reset loop
      %s -- show as function call
    else
      __firstIteration = nil
    end

    local __vars = table.pack(%s)

    -- name list
    local %s = table.unpack(__vars, 1, __vars.n)

    %s -- eval names

    if __vars[1] == nil then break end
    __var = __vars[1]

    -- block
    %s
  end
end
]]):format(exprs,
           hshowCall("__f", {"__s", "__var"}, exprSlice),
           #node.data.block > 0 and hreset(blockSlice) or "",
           hs(hshowCall("__f", {"__s", "__var"}, exprSlice),
              node.data.topSlice),
           hcall("__f", nil, "(__s, __var)", exprSlice),
           names,
           evalNames,
           block), node.data.topSlice)
      end
    elseif node.ty == "assign" then
      local ret = ""

      for i, var in ipairs(node.data.vars) do
        if i > 1 then
          ret = ret .. ", "
        end

        ret = ret .. showNode(var)
      end

      ret = ret .. " = "

      if node.data.func then
        ret = ret .. showNode(node.data.exps[1])
      else
        ret = ret .. hexprList(node.data.exps, #node.data.vars)
      end

      return hs(ret, node)
    elseif node.ty == "decl" then
      local ret = "local "

      for i, name in ipairs(node.data.names) do
        if i > 1 then
          ret = ret .. ", "
        end

        ret = ret .. showNode(name)
      end

      if node.data.exps then
        ret = ret .. " = " .. hexprList(node.data.exps, #node.data.names)
      end

      return hs(ret, node)
    elseif node.ty == "funcbody" then
      local ret = "function("

      for i, param in ipairs(node.data.pars) do
        if i > 1 then
          ret = ret .. ", "
        end

        ret = ret .. showNode(param)
      end

      ret = ret .. ")\n" .. hs("", node.data.topSlice)

      --for i, param in ipairs(node.data.pars) do
      --  ret = ret .. "\n" .. heval(showNode(param), param.slice)
      --end
      ret = ret .. hexprList(node.data.pars, true, ";") .. ";"

      local hasReturn = false

      for i, stat in ipairs(node.data.block) do
        if stat.ty == "return" then
          hasReturn = true
        end

        ret = ret .. "\n" .. showNode(stat, true) .. ";"
      end

      if not hasReturn then
        ret = ret .. "\n;" .. hreturn("",
                                      {slice = {node.slice[3], node.slice[4],
                                                node.slice[4], node.slice[4]}})
      end

      return registerFunc(ret .. "\nend", node, node.data.name)
    end
  end

  h = function(s, node, allowMany)
    if not node then
      node = s
      s = showNode(node)
    end

    if (node.ty == "number" or
        node.ty == "boolean" or
        node.ty == "string" or
        node.ty == "nil" or
        node.ty == "table") then
      return s
    end

    local slice = node.slice or node

    if node.ty == "funcbody" then
      slice = node.data.fullSlice
    end

    if not allowMany then
      s = "(" .. s .. ")"
    end

    return (
    [[
highlight({%d, %d}, {%d, %d}, (function(...)
  local values = table.pack(...)

  return function()
    return table.unpack(values, 1, values.n)
  end
end)(%s), 'expr')]]
  ):format(slice[1], slice[2], slice[3], slice[4], s)
  end

  hexprList = function(exprs, extra, separator)
    local values = {}

    for i, expr in ipairs(exprs) do
      if i == #exprs and extra and (expr.ty == "call" or
                                    expr.ty == "dots" or
                                    expr.ty == "methodCall") then
        local prevExpr = exprs[i - 1]

        if prevExpr then
          prevExpr = {prevExpr.slice[3], prevExpr.slice[4] + 1}
        else
          prevExpr = expr.slice
        end

        local slice = expr.slice

        table.insert(values, ([[
highlight({%d, %d}, {%d, %d}, (function(...)
local values = table.pack(...)

return function()
  return table.unpack(values, 1, %s)
end
end)(%s), 'manyExpr', {%d, %d})
]]):format(slice[1], slice[2], slice[3], slice[4],
           extra == true and "values.n" or tostring(extra - i + 1),
           showNode(expr),
           prevExpr[1], prevExpr[2]))
      else
        table.insert(values, h(expr))
      end
    end

    return table.concat(values, separator or ", ")
  end

  hs = function(s, slice, isExpr)
    if not slice then
      slice = s
      s = showNode(slice)
    end

    if slice.slice then
      slice = slice.slice
    end

    if isExpr then
      return ([[
(function()
  highlight({%d, %d}, {%d, %d}, nil, 'stmt')
  return %s
end)()]]):format(slice[1], slice[2], slice[3], slice[4], s)
    else
      return ("highlight({%d, %d}, {%d, %d}, nil, 'stmt'); %s"):format(
        slice[1], slice[2], slice[3], slice[4], s
      )
    end
  end

  heval = function(expr, slice)
    return ([[
highlight({%d, %d}, {%d, %d}, (function(...)
  local values = table.pack(...)

  return function()
    return table.unpack(values, 1, values.n)
  end
end)(%s), 'eval')]]):format(
      slice[1], slice[2], slice[3], slice[4],
      expr
    )
  end

  hreset = function(slice)
    return (";highlight({%d,%d}, {%d,%d}, nil, 'reset');"):format(
      slice[1], slice[2], slice[3], slice[4]
    )
  end

  hcall = function(callable, method, args, node)
    local slice = node.slice

    if not slice then
      slice = node
    end

    if method then
      return ([[((function(object, callable)
  return function(...)
    local args = table.pack(...)

    if callable == coroutine.yield or callable == coroutine.resume then
      highlight({%d, %d}, {%d, %d}, nil, 'show-call')
    else
      local slice = __registeredFuncs[callable]

      if slice then
        highlight({%d, %d}, {%d, %d}, slice[3], 'call', slice[1], slice[2])
      end
    end

    return callable(object, table.unpack(args, 1, args.n))
  end
end)((function()
  local object = %s
  local callable = object.%s

  return object, callable
end)()))%s]]):format(
                 slice[1], slice[2], slice[3], slice[4],
                 slice[1], slice[2], slice[3], slice[4],
                 callable, method, args)
    else
      return ([[((function(callable)
  return function(...)
    local args = table.pack(...)

    if callable == coroutine.yield or callable == coroutine.resume then
      highlight({%d, %d}, {%d, %d}, nil, 'show-call')
    else
      local slice = __registeredFuncs[callable]

      if slice then
        highlight({%d, %d}, {%d, %d}, slice[3], 'call', slice[1], slice[2])
      end
    end

    return callable(table.unpack(args, 1, args.n))
  end
end)(%s))%s]]):format(
                 slice[1], slice[2], slice[3], slice[4],
                 slice[1], slice[2], slice[3], slice[4],
                 callable, args)
    end
  end

  hreturn = function(values, node)
    return ([[(function(...)
  local values = table.pack(...)

  highlight({%d, %d}, {%d, %d}, nil, 'return')

  return table.unpack(values, 1, values.n)
end)(%s)]]):format(node.slice[1], node.slice[2], node.slice[3], node.slice[4],
                   values)
  end

  registerFunc = function(body, node, name)
    return([[(function(f)
  __registeredFuncs[f] = {{%d, %d}, {%d, %d}, %s}
  return f
end)(%s)]]):format(node.slice[1], node.slice[2], node.slice[3], node.slice[4],
                   name and ("%q"):format(showNode(name)) or "nil", body)
  end

  hshowCall = function(f, args, slice)
    return ("highlight({%d, %d}, {%d, %d}, %s, 'makeCall', %s)"):format(
      slice[1], slice[2], slice[3], slice[4],
      f, table.concat(args, ", ")
    )
  end

  return showNode(node, true)
end

local reservedWords = {"and", "break", "do", "else", "elseif", "end", "false",
                       "for", "function", "goto", "if", "in", "local", "nil",
                       "not", "or", "repeat", "return", "then", "true", "until",
                       "while"}

for _, word in ipairs(reservedWords) do
  reservedWords[word] = true
end

local insertNonNil = function(t, v)
  if v then
    v = tostring(v)

    if #v > 0 then
      table.insert(t, v)
    end
  end
end

local formatExpr

local function formatIndex(tbl, field, env)
  if type(field) == "string" and field:match("^[a-zA-Z_][a-zA-Z0-9_]*") and
      not reservedWords[field] then
    if tbl == "_G" then
      return field
    else
      return tbl .. "." .. field
    end
  else
    return tbl .. "[" .. formatExpr({field}, env) .. "]"
  end
end

local function collectGlobalFunctions(env)
  env = env or _G
  local result = {}
  local queue = {{"_G", env}}
  local visited = {}

  while #queue > 0 do
    local value = table.remove(queue, 1)
    local name, value = table.unpack(value)

    if type(value) == "function" and not result[value] then
      result[value] = name
    elseif type(value) == "table" and not visited[value] then
      visited[value] = true

      for k, v in pairs(value) do
        table.insert(queue, {formatIndex(name, k, env), v})
      end
    end
  end

  return result
end

local function getName(f, env, callLevel)
  if env.__registeredFuncs[f] then
    return env.__registeredFuncs[f][3], env.__registeredFuncs[f][1][1]
  elseif env.__globalFuncs[f] then
    return env.__globalFuncs[f]
  end

  -- look through locals
  -- (care has to be taken to ignore locals not defined by processed code,
  --  e.g. `formatExpr`'s locals: can be done easily by checking source)
  -- start from frame 2 nonetheless, since 0 = debug.getinfo, 1 = getName
  callLevel = callLevel or 2

  for level = callLevel, math.huge, 1 do
    local frame = debug.getinfo(level, "Snf")

    if not frame then
      break
    elseif frame.source == "=compiled" then
      local index = 1
      local step = "local" -- "local" -> "vararg" -> "upvalue"

      while true do
        local name, value

        if step == "local" then
          name, value = debug.getlocal(level, index)
        elseif step == "vararg" then
          name, value = debug.getlocal(level, -index)
        elseif step == "upvalue" then
          name, value = debug.getupvalue(frame.func, index)
        else
          error("non-exhausting matching")
        end

        if not name then
          if step == "local" then
            -- try searching through varargs
            index = 1
            step = "vararg"
          elseif step == "vararg" and frame.func then
            -- what about upvalues?
            index = 1
            step = "upvalue"
          else
            -- oh well!
            break
          end
        elseif value == f and name ~= "__f" then
          if step == "local" or step == "upvalue" then
            return name
          else
            -- this is fun: we need name of function on this level first
            local thisLevelName = frame.name

            if not thisLevelName or frame.name == "__f" then
              -- recurse deeper
              thisLevelName = getName(frame.func, env, level + 1)
            end

            if not thisLevelName then
              -- we're left with empty hands even after all this hassle!
              thisLevelName = "<anon>"

              if frame.linedefined then
                thisLevelName = thisLevelname .. ":" .. frame.linedefined
              end
            end

            return ("<vararg #%d of %s>"):format(index, thisLevelName)
          end
        end

        index = index + 1
      end

      -- don't mess with unreachable values
      return
    end
  end
end

function formatExpr(expr, env)
  local t = {}

  for k = 1, expr.n or #expr, 1 do
    local v = expr[k]

    if type(v) == "string" then
      local truncated = false

      v = '"' .. stringq(v) .. '"'

      if utf8.len(v) > 21 then
        if v:sub(utf8.offset(v, 20), utf8.offset(v, 20)) == "\\" then
          v = v:sub(1, utf8.offset(v, 21)) .. '…"'
        else
          v = v:sub(1, utf8.offset(v, 20)) .. '…"'
        end
      end
    elseif type(v) == "number" or
        type(v) == "boolean" or
        type(v) == "nil" then
      v = tostring(v)
    elseif type(v) == "table" then
      local numeric, nonNumeric, nonNumericStripped = {}, {}, {}

      for i, item in ipairs(expr[k]) do
        table.insert(numeric, formatExpr({item}, env))
      end

      for idx, item in pairs(expr[k]) do
        if not numeric[idx] then
          local entry

          if type(idx) == "string" and idx:match("^[a-zA-Z_][a-zA-Z0-9_]*$") and
              not reservedWords[idx] then
            entry = idx
          else
            entry = "[" .. formatExpr({idx}, env) .. "]"
          end

          local value = formatExpr({item}, env)

          if utf8.len(value) > 21 then
            table.insert(nonNumericStripped, entry .. " = …")
          else
            table.insert(nonNumeric, entry .. " = " .. value)
          end
        end
      end

      table.sort(nonNumeric, function(lhs, rhs)
        return utf8.len(lhs) < utf8.len(rhs)
      end)

      table.sort(nonNumericStripped, function(lhs, rhs)
        return utf8.len(lhs) < utf8.len(rhs)
      end)

      table.move(nonNumericStripped, 1, #nonNumericStripped, #nonNumeric + 1,
                 nonNumeric)

      local outNum = 0
      local outNonNum = 0

      for i = 1, #numeric + #nonNumeric do
        if (i % 2 == 1 or outNonNum == #nonNumeric) and outNum < #numeric then
          outNum = outNum + 1
        elseif (i % 2 == 0 or outNum == #numeric) and outNonNum < #nonNumeric then
          outNonNum = outNonNum + 1
        end

        if (utf8.len(table.concat(numeric, ", ", 1, outNum)) +
            utf8.len(table.concat(nonNumeric, ", ", 1, outNonNum)) > 21) then
          break
        end
      end

      if outNum < #numeric then
        table.insert(numeric, outNum + 1, "…")
        outNum = outNum + 1
      end

      if outNonNum < #nonNumeric then
        table.insert(nonNumeric, outNonNum + 1, "…")
        outNonNum = outNonNum + 1
      end

      v = "{" .. table.concat(numeric, ", ", 1, outNum)

      if outNum > 0 and outNonNum > 0 then
        v = v .. "; "
      end

      v = v .. table.concat(nonNumeric, ", ", 1, outNonNum) .. "}"
    elseif type(v) == "function" then
      local info = debug.getinfo(v)

      local funcType, name, args, exec, defined = {}, nil, {}, ""

      if info.source == "=compiled" and info.name ~= "__f" then
        name = info.name
      end

      if not name and env then
        name, defined = getName(v, env)

        if defined then
          defined = ":" .. defined
        end
      end

      name = name or "function"

      if info.nparams then
        for an = 1, info.nparams do
          local argName, argValue = debug.getlocal(v, an)

          if argValue ~= nil then
            argName = argName .. "=" .. i(argValue)
          end

          table.insert(args, argName)
        end
      end

      if info.isvararg then
        table.insert(args, "...")
      end

      if not defined and info.short_src == "compiled" then
        if info.linedefined and info.linedefined ~= -1 then
          defined = ":" .. info.linedefined
        end
      end

      if not defined then
        defined = ""
      end

      funcType = table.concat(funcType, " ")
      args = table.concat(args, ", ")

      v = ("<%s(%s)%s>"):format(
        name,
        #args > 0 and args or "",
        defined
      )
    elseif type(v) == "thread" then
      local n = env.__runningCoroutines[v]

      if n then
        v = ("<coroutine #%d>"):format(n)
      elseif v == env.__mainThread then
        v = "<main thread>"
      else
        v = "<" .. tostring(v) .. ">"
      end
    elseif type(v) == "userdata" then
      v = "<userdata>"
    end

    t[k] = v
  end

  return table.concat(t, ", ")
end

if mark then
  os.exit()
end

local passError = false

local success, msg = xpcall(function()
  local code = "__mainThread = coroutine.running()\n\n"

  for _, stmt in ipairs(block) do
    code = code .. compile(stmt) .. ";\n"
  end

  if args[3] then
    local f = io.open(args[3], "w")
    f:write(code)
    f:close()
  end

  local f = assert(io.open(args[2], "w"))

  local env

  do
    local function createCoroutine(...)
      local f = ...
      local co = coroutine.create(f)
      local info = env.__registeredFuncs[f]

      if info then
        env.__runningCoroutines.n = env.__runningCoroutines.n + 1
        env.__runningCoroutines[co] = env.__runningCoroutines.n
        env.highlight(info[1], info[2], info[3], "new-coroutine",
                      env.__runningCoroutines.n)
      end

      return co
    end

    local function resumeCoroutine(...)
      local co = ...

      local coroutineNumber = env.__runningCoroutines[co]

      if (not coroutineNumber or -- not tracked by us
          coroutine.status(co) ~= "suspended") -- no interest in the dead
          then
        return coroutine.resume(...)
      end

      -- notify before resuming!
      env.highlight(nil, nil, coroutineNumber, "coroutine")
      local returnedData = table.pack(
        coroutine.resume(...)
      )

      -- also notify
      local runningCoNumber = 0

      do
        local co, main = coroutine.running()

        if not main then
          runningCoNumber = env.__runningCoroutines[co]
        end
      end

      assert(runningCoNumber)

      env.highlight(nil, nil, runningCoNumber, "coroutine")

      return table.unpack(returnedData, 1, returnedData.n)
    end

    local function wrapCoroutine(...)
      local f = ...
      local co = env.coroutine.create(f)

      return function(...)
        local returnedData = table.pack(env.coroutine.resume(co, ...))

        if not returnedData[1] then
          error(returnedData[2])
        else
          return table.unpack(returnedData, 2, returnedData.n)
        end
      end
    end

    env = setmetatable({
      coroutine = setmetatable({}, {
        __index = setmetatable({
          create = createCoroutine,
          resume = resumeCoroutine,
          wrap = wrapCoroutine
        }, {__index = coroutine, __metatable = "blocked"}),
        __pairs = function(self)
          return function(_, key)
            local k, v = next(coroutine, key)

            return k, env.coroutine[k]
          end, nil, nil
        end,
        __metatable = "blocked"
      }),
    }, {
      __index = _G,
      __pairs = function(self)
        return function(_, key)
          local k = next(_G, key)

          return k, env[k]
        end
      end,
      __metatable = "blocked"
    })
  end

  env.__globalFuncs = collectGlobalFunctions(env)

  env.highlight = function(p1, p2, expr, htype, f1, f2)
    if htype == "expr" or htype == "eval" then
      local values = table.pack(expr())

      f:write(("%d,%d %d,%d expr %s\n"):format(
        p1[1], p1[2], p2[1], p2[2], formatExpr(values, env)
      ))

      return table.unpack(values, 1, values.n)
    elseif htype == "manyExpr" then
      local values = table.pack(expr())

      if values.n == 0 then
        f:write(("%d,%d %d,%d expr \n"):format(f1[1], f1[2], p2[1], p2[2]))
      else
        f:write(("%d,%d %d,%d expr %s\n"):format(
          p1[1], p1[2], p2[1], p2[2], formatExpr(values, env)
        ))
      end

      return table.unpack(values, 1, values.n)
    elseif htype == "makeCall" then
      f:write(("%d,%d %d,%d makecall %s(%s)\n"):format(
        p1[1], p1[2], p2[1], p2[2],
        formatExpr({expr}, env), formatExpr({f1, f2}, env)
      ))
    elseif htype == "reset" then
      f:write(("%d,%d %d,%d reset\n"):format(p1[1], p1[2], p2[1], p2[2]))
    elseif htype == "stmt" then
      f:write(("%d,%d %d,%d stmt\n"):format(p1[1], p1[2], p2[1], p2[2]))
    elseif htype == "call" then
      f:write(("%d,%d %d,%d call %d,%d %d,%d%s\n"):format(
        p1[1], p1[2], p2[1], p2[2],
        f1[1], f1[2], f2[1], f2[2],
        expr and (" " .. expr) or ""
      ))
    elseif htype == "show-call" then
      f:write(("%d,%d %d,%d showcall\n"):format(p1[1], p1[2], p2[1], p2[2]))
    elseif htype == "return" then
      f:write(("%d,%d %d,%d return\n"):format(p1[1], p1[2], p2[1], p2[2]))
    elseif htype == "new-coroutine" then
      f:write(("%d,%d %d,%d newco %d%s\n"):format(
        p1[1], p1[2], p2[1], p2[2],
        f1,
        expr and (" " .. expr) or ""
      ))
    elseif htype == "coroutine" then
      f:write(("-1,-1 -1,-1 co %d\n"):format(expr))
    else
      error("unknown highlight event type")
    end
  end

  env.mark = true
  env.__registeredFuncs = {}
  env.__runningCoroutines = {n = 0}

  assert(load(code, "=compiled", "t", env))()
end, function(msg)
  passError = debug.getinfo(3).source == "=compiled"
  return traceback(msg)
end)

if not success then
  io.stderr:write(msg .. "\n")

  if not passError then
    os.exit(1)
  end
end
