local class = require("30log")

local function hashset(...)
  local s = {}

  for _, v in ipairs({...}) do
    s[v] = true
  end

  return s
end

local Lexer = class("Lexer")

function Lexer:init(source, file, colored)
  self.pos = 1
  self.col = 1
  self.line = 1
  self.source = source
  self.file = file or "<unnamed>"
  self.colored = type(colored) == "boolean" and colored or true
  self.highlight = {}
end

function Lexer:peek(o)
  o = o or 0
  return self.source:sub(
    self.pos + o,
    (utf8.offset(self.source, 2, self.pos + o) or (self.pos + o + 1)) - 1)
end

function Lexer:next()
  if self.nsaved then
    self.nsaved = nil
    return "\n"
  end

  local c = self.source:sub(self.pos, utf8.offset(self.source, 2, self.pos) - 1)
  self.pos = self.pos + #c
  self.col = self.col + 1

  if c == "\r" or c == "\n" then
    if c == "\r" and self:peek() == "\n" then
      self.pos = self.pos + 1
      self.nsaved = true
    end

    self.col = 1
    self.line = self.line + 1
  end

  return c
end

function Lexer:pmatch(pattern)
  local match = {self.source:sub(self.pos):find("^" .. pattern)}
  if #match == 0 then return end

  local s, e = table.remove(match, 1), table.remove(match, 1)

  return self.source:sub(self.pos + s - 1, self.pos + e - 1), table.unpack(match)
end

function Lexer:match(pattern)
  local matched = {self:pmatch(pattern)}

  if #matched > 0 then
    self.pos = self.pos + #matched[1]
    self.col = self.col + #matched[1]

    return table.unpack(matched)
  end
end

function Lexer:getline(nth)
  if nth < 1 then return end

  local i = 1

  for cur in self.source:gmatch("([^\r\n]*)\r?\n?") do
    if i == nth then
      return cur
    end

    i = i + 1
  end
end

function Lexer:showSlice(slice, color)
  if self.colored then
    for _ in self:lex() do end
  end

  local pad = math.ceil(math.log(math.max(slice[1], slice[3]), 10))
  if pad == 0 then pad = 1 end

  if self.colored then
    print(("\27[1;34m%s-->\27[0m %s:%d:%d")
        :format((" "):rep(pad), self.file, slice[1], slice[2]))
  else
    print(("%s--> %s:%d:%d")
        :format((" "):rep(pad), self.file, slice[1], slice[2]))
  end

  for i = slice[1] - 2, slice[3] + 2 do
    local line = self:getline(i)
    if not line then goto continue end

    if self.colored then
      io.write(("\27[1;34m%" .. pad .. "d |\27[0m "):format(i))

      local ci = 1

      while #line > 0 do
        local char = line:sub(1, utf8.offset(line, 2) - 1)

        if self.highlight[i] and self.highlight[i][ci] then
          io.write("\27[0m" .. self.highlight[i][ci])
        end

        io.write(char)

        ci = ci + 1
        line = line:sub(utf8.offset(line, 2))
      end

      print("\27[0m")
    end

    if i >= slice[1] and i <= slice[3] then
      if self.colored then
        io.write("\27[1;34m")
      end

      io.write((" "):rep(pad + 1) .. "| ")

      if self.colored then
        io.write("\27[31m")
      end

      io.write((" "):rep(i == slice[1] and (slice[2] - 1) or 0))
      io.write(("~"):rep((i == slice[3] and slice[4] or (utf8.len(line) + 1))
                       - (i == slice[1] and slice[2] or 1)))
      io.write((i == slice[3] and "^" or ""))

      if self.colored then
        io.write("\27[0m")
      end

      io.write("\n")
    end

    ::continue::
  end

  return pad
end

function Lexer:error(message, slice)
  coroutine.yield("error", message, slice)
end

Lexer.COLORS = {
  keyword = "\27[1;36m",
  punct = "\27[1;37m",
  number = "\27[33m",
  string = "\27[32m",
  name = "",
  comment = "\27[37m"
}

function Lexer:pushcolor(ty, slice)
  if not self.colored then
    return
  end

  if Lexer.KEYWORDS[ty] then
    ty = "keyword"
  end

  if Lexer.OPS[3][ty] then
    ty = "punct"
  end

  local color = Lexer.COLORS[ty]

  for line = slice[1], slice[3] do
    if not self.highlight[line] then
      self.highlight[line] = {}
    end

    self.highlight[line][line == slice[1] and slice[2] or 1] = color
  end
end

function Lexer:token(ty, data, slice)
  if not self.colored then
    coroutine.yield(ty, data, slice)
    return
  end


  self:pushcolor(ty, slice)
  coroutine.yield(ty, data, slice)
end

Lexer.WHITESPACE = hashset(" ", "\t", "\n", "\v", "\f", "\r")

Lexer.KEYWORDS = hashset(
  "and", "break", "do", "else", "elseif", "end",
  "false", "for", "function", "goto", "if", "in",
  "local", "nil", "not", "or", "repeat", "return",
  "then", "true", "until", "while"
)

Lexer.OPS = {}

Lexer.OPS[3] = hashset(
     "+", "-", "*", "/", "%", "^", "#",
     "&", "~", "|", "<<", ">>", "//",
     "==", "~=", "<=", ">=", "<", ">", "=",
     "(", ")", "{", "}", "[", "]", "::",
     ";", ":", ",", ".", "..", "..."
)

Lexer.OPS[1] = {} do
  for k in pairs(Lexer.OPS[3]) do
    Lexer.OPS[1][k:sub(1, 1)] = true
  end
end

Lexer.OPS[2] = {} do
  for k in pairs(Lexer.OPS[3]) do
    Lexer.OPS[2][k:sub(1, 2)] = true
  end
end

function Lexer.isnamehead(c)
  return (c >= "a" and c <= "z") or (c >= "A" and c <= "Z") or c == "_"
end

function Lexer.isdigit(c)
  return (c >= "0" and c <= "9")
end

function Lexer:lexc()
  local operator = ""

  while self:peek() ~= "" do
    local c = self:peek()

    if self:pmatch("%-%-") then
      self:comment()
      goto continue
    end

    if self:pmatch("%[=*%[") then
      self:longstring()
      goto continue
    end

    if #operator == 0 and Lexer.OPS[1][c] then
      self.broken = nil
      operator = operator .. c
      self:next()
      goto continue
    elseif #operator == 1 and Lexer.OPS[2][operator .. c] and not self.broken then
      operator = operator .. c
      self:next()
      goto continue
    elseif #operator == 2 and Lexer.OPS[3][operator .. c] and not self.broken then
      operator = operator .. c
      self:next()
      goto continue
    elseif #operator > 0 then
      self:token(operator, nil,
          {self.line, self.col - #operator, self.line, self.col - 1})
      operator = ""
      self.broken = nil
    end

    if #operator == 0 and Lexer.OPS[1][c] then
      self.broken = nil
      operator = operator .. c
      self:next()
      goto continue
    elseif #operator == 1 and Lexer.OPS[2][operator .. c] and not self.broken then
      operator = operator .. c
      self:next()
      goto continue
    elseif #operator == 2 and Lexer.OPS[3][operator .. c] and not self.broken then
      operator = operator .. c
      self:next()
      goto continue
    end

    if Lexer.WHITESPACE[c] then
      self:next()
    elseif c == "'" or c == '"' then
      self:string()
    elseif Lexer.isnamehead(c) then
      self:name()
    elseif Lexer.isdigit(c) or (c == "." and Lexer.isdigit((self:peek(1)))) then
      self:number()
    else
      self:error(("unexpected symbol `%s`"):format(c),
        {self.line, self.col, self.line, self.col})
    end

    ::continue::
  end

  if #operator > 0 then
    self:token(operator, nil, {self.line, self.col - 1, self.line, self.col - 1})
  end
end

function Lexer:name()
  local startLine, startCol = self.line, self.col
  local match = self:match("[a-zA-Z_][a-zA-Z0-9_]*")

  if Lexer.KEYWORDS[match] then
    self:token(match, nil, {startLine, startCol, self.line, self.col - 1})
  else
    self:token("name", match, {startLine, startCol, self.line, self.col - 1})
  end
end

function Lexer:number()
  local startLine, startCol = self.line, self.col

  local s
  s =      self:match("0[xX]%x+%.?%x*[eEpP][+-]?%d+")
  s = s or self:match("0[xX]%x+%.?%x*")
  s = s or self:match("0[xX]%x*%.?%x+[eEpP][+-]?%d+")
  s = s or self:match("0[xX]%x*%.?%x+")
  s = s or self:match("%d+%.?%d*[eE][+-]?%d+")
  s = s or self:match("%d+%.?%d*")
  s = s or self:match("%d*%.?%d+[eE][+-]?%d+")
  s = s or self:match("%d*%.?%d+")

  if Lexer.isnamehead(self:peek()) then
    s = nil
  end

  if s then
    self:token("number", tonumber(s), {startLine, startCol, self.line, self.col - 1})
  else
    self:error("malformed number", {startLine, startCol, startLine, startCol})
  end
end

function Lexer:comment()
  self.broken = true
  local startLine, startCol = self.line, self.col

  self:next()
  self:next()

  local x, level = self:match("%[(=*)%[")

  if not level then
    self:match("[^\r\n]*")
    self:pushcolor("comment", {startLine, startCol, self.line, self.col})
    return
  else
    local m = self:match("(.-)%]" .. level .. "%]")
    self:pushcolor("comment", {startLine, startCol, self.line, self.col})
    if m then return end
  end

  self:error("unfinished long comment",
             {startLine, startCol, startLine, startCol + 3 + #level})
end

Lexer.CESCAPES = {
  ["a"] = "\a",
  ["b"] = "\b",
  ["f"] = "\f",
  ["n"] = "\n",
  ["r"] = "\r",
  ["t"] = "\t",
  ["v"] = "\v",
  ["\\"] = "\\",
  ["\""] = "\"",
  ["'"] = "'",
}

function Lexer:longstring()
  local startLine, startCol = self.line, self.col

  local _, level = self:match("%[(=*)%[")
  local _, inner = self:match("(.-)%]" .. level .. "%]")

  if inner then
    self:token("string", inner, {startLine, startCol, self.line, self.col})
  else
    self:error("unfinished long string",
               {startLine, startCol, startLine, startCol + 1 + #level})
  end
end

function Lexer:string()
  local startLine, startCol = self.line, self.col
  local pos = 1

  local q = self:next()
  local escaped = false

  local acc = ""

  while true do
    local c = self:peek()

    if c == "" then
      self:error(("`%s` expected to close the string, found EOF"):format(q),
                 {startLine, startCol + pos, startLine, startCol})
    end

    if not escaped then
      if c == "\\" then
        escaped = true
      elseif c == q then
        self:next()
        self:token("string", acc, {startLine, startCol, self.line, self.col - 1})
        return
      elseif c == "\n" or c == "\r" then
        self:error(("`%s` expected to close the string, found EOL"):format(q),
                   {startLine, startCol + pos, startLine, startCol + pos})
      else
        acc = acc .. c
      end
    elseif Lexer.CESCAPES[c] then
      acc = acc .. Lexer.CESCAPES[c]
      escaped = false
    elseif c == "z" then
      self:next()
      pos = pos + 1

      while Lexer.WHITESPACE[self:peek()] do
        self:next()
        pos = pos + 1
      end

      escaped = false

      goto continue
    elseif c == "x" then
      self:next()
      pos = pos + 1

      local code = self:match("%x%x")

      if not code then
        self:error(
          "invalid hexadecimal escape sequence",
          {startLine, startCol + pos - 2, startLine, startCol + pos - 1}
        )
      end

      pos = pos + 2
      acc = acc .. string.char(tonumber(code, 16))
      escaped = false

      goto continue
    elseif Lexer.isdigit(c) then
      local code = self:match("%d%d?%d?")

      if tonumber(code) > 255 then
        self:error(
          "invalid decimal escape sequence (too large)",
          {startLine, startCol + pos - 1, startLine, startCol + pos + #code - 1}
        )
      end

      pos = pos + #code
      acc = acc .. string.char(tonumber(code))
      escaped = false

      goto continue
    elseif c == "u" then
      self:next()
      pos = pos + 1

      local code, digits = self:match("{(%x+)}")

      if not code then
        self:error(
          "invalid UTF-8 escape sequence",
          {startLine, startCol + pos - 2, startLine, startCol + pos - 1}
        )
      end

      if not pcall(utf8.char, tonumber(digits, 16)) then
        self:error(
          "invalid UTF-8 escape sequence (out of range)",
          {startLine, startCol + pos - 2, startLine, startCol + pos + #code - 1}
        )
      end

      pos = pos + #code
      acc = acc .. utf8.char(tonumber(digits, 16))
      escaped = false

      goto continue
    else
      self:error(
        "invalid escape sequence",
        {startLine, startCol + pos - 1, startLine, startCol + pos - 1}
      )
    end

    self:next()
    pos = pos + 1

    ::continue::
  end
end

function Lexer:lex()
  local co = coroutine.create(function() return self:lexc() end)
  local dead = false

  return function()
    local res = {coroutine.resume(co)}

    if not dead and table.remove(res, 1) then
      if res[1] == "error" then
        dead = true
      end

      return res[1], res[2], res[3]
    end
  end
end

local Parser = class("Parser")

function Parser:init(lexer)
  self.l = lexer
  self.tokens = self.l:lex()
  self.saved = {}
  self.pos = 1
end

function Parser:peek()
  if not self.saved[self.pos] then
    self.saved[self.pos] = {self.tokens()}
  end

  return table.unpack(self.saved[self.pos])
end

function Parser:next()
  local ret = {self:peek()}

  if self.saving then
    self.pos = self.pos + 1
    self.previous = self.saved[self.pos - 2]
  else
    self.previous = table.remove(self.saved, self.pos)
  end

  return table.unpack(ret)
end

function Parser:save()
  self.saving = true
end

function Parser:restore()
  self.saving = false
  self.pos = 1
end

function Parser:commit()
  self.saved = {self.saved[self.pos]}
  self.saving = false
  self.pos = 1
end

function Parser.node(ty, data, slice)
  return {ty = ty, data = data, slice = slice}
end

local function stringq(s)
  return s:gsub("\\", "\\\\"):gsub("\n", "\\n"):gsub("\r", "\\r"):gsub("'", "\\'")
    :gsub("\t", "\\t"):gsub("\v", "\\v"):gsub("\f","\\f"):gsub("\a", "\\a")
    :gsub("\b", "\\b")
end

function Parser:showNode(node)
  local function showNode(node)
    if node.ty == "name" then
      return node.data
    elseif node.ty == "number" then
      return tostring(node.data)
    elseif node.ty == "nil" then
      return "nil"
    elseif node.ty == "boolean" then
      return tostring(node.data)
    elseif node.ty == "string" then
      return ("('%s')"):format(stringq(node.data))
    elseif node.ty == "dots" then
      return "..."
    elseif node.ty == "binop" then
      return "(" .. showNode(node.data.lhs) .. " " .. node.data.op .. " "
          .. showNode(node.data.rhs) .. ")"
    elseif node.ty == "unop" then
      return node.data.op .. " " .. showNode(node.data.node)
    elseif node.ty == "index" then
      return showNode(node.data.indexing) .. "["
        .. showNode(node.data.index) .. "]"
    elseif node.ty == "call" then
      local ret = showNode(node.data.callable) .. "("

      for i, arg in ipairs(node.data.args) do
        if i > 1 then
          ret = ret .. ", "
        end

        ret = ret .. showNode(arg)
      end

      return ret .. ")"
    elseif node.ty == "methodcall" then
      local ret = showNode(node.data.callable) .. ":"
               .. showNode(node.data.method) .. "("

      for i, arg in ipairs(node.data.args) do
        if i > 1 then
          ret = ret .. ", "
        end

        ret = ret .. showNode(arg)
      end

      return ret .. ")"
    elseif node.ty == "table" then
      local ret = "{"

      local shown = {}

      for i, v in ipairs(node.data.seq) do
        if i > 1 then
          ret = ret .. ", "
        end
        ret = ret .. showNode(v)
      end

      for k, v in pairs(node.data.map) do
        ret = ret .. "[" .. showNode(k) .. "] = " .. showNode(v) .. ", "
      end

      return ret .. "}"
    elseif node.ty == "emptystat" then
      return ";"
    elseif node.ty == "do" then
      local ret = "do"

      for i, stat in ipairs(node.data) do
        ret = ret .. "\n" .. showNode(stat)
      end

      return ret .. "\nend"
    elseif node.ty == "if" then
      local ret = "if " .. showNode(node.data.cond) .. " then"

      for i, stat in ipairs(node.data.block) do
        ret = ret .. "\n" .. showNode(stat)
      end

      for i, branch in ipairs(node.data.pelseif) do
        ret = ret .. "\nelseif " .. showNode(branch.cond) .. " then"

        for i, stat in ipairs(branch.block) do
          ret = ret .. "\n" .. showNode(stat)
        end
      end

      if node.data.pelse then
        ret = ret .. "\nelse"

        for i, stat in ipairs(node.data.pelse) do
          ret = ret .. "\n" .. showNode(stat)
        end
      end

      return ret .. "\nend"
    elseif node.ty == "while" then
      local ret = "while " .. showNode(node.data.cond) .. " do"

      for i, stat in ipairs(node.data.block) do
        ret = ret .. "\n" .. showNode(stat)
      end

      return ret .. "\nend"
    elseif node.ty == "repeat" then
      local ret = "repeat"

      for i, stat in ipairs(node.data.block) do
        ret = ret .. "\n" .. showNode(stat)
      end

      return ret .. "\nuntil " .. showNode(node.data.cond)
    elseif node.ty == "goto" then
      return "goto " .. showNode(node.data)
    elseif node.ty == "label" then
      return "::" .. showNode(node.data) .. "::"
    elseif node.ty == "break" then
      return "break"
    elseif node.ty == "return" then
      local ret = "return "

      for i, v in ipairs(node.data) do
        if i > 1 then
          ret = ret .. ", "
        end

        ret = ret .. showNode(v)
      end

      return ret
    elseif node.ty == "for" then
      local ret = "for "

      if node.data.counter then
        ret = ret .. showNode(node.data.counter) .. " = "
                  .. showNode(node.data.init) .. ", "
                  .. showNode(node.data.limit) .. (node.data.step
                      and (", " .. showNode(node.data.step)) or "")
      else
        for i, name in ipairs(node.data.names) do
          if i > 1 then
            ret = ret .. ", "
          end

          ret = ret .. showNode(name)
        end

        ret = ret .. " in "

        for i, expr in ipairs(node.data.exps) do
          if i > 1 then
            ret = ret .. ", "
          end

          ret = ret .. showNode(expr)
        end
      end

      ret = ret .. " do"

      for i, stat in ipairs(node.data.block) do
        ret = ret .. "\n" .. showNode(stat)
      end

      return ret .. "\nend"
    elseif node.ty == "assign" then
      local ret = ""

      for i, var in ipairs(node.data.vars) do
        if i > 1 then
          ret = ret .. ", "
        end

        ret = ret .. showNode(var)
      end

      ret = ret .. " = "

      for i, expr in ipairs(node.data.exps) do
        if i > 1 then
          ret = ret .. ", "
        end

        ret = ret .. showNode(expr)
      end

      return ret
    elseif node.ty == "decl" then
      local ret = "local "

      for i, name in ipairs(node.data.names) do
        if i > 1 then
          ret = ret .. ", "
        end

        ret = ret .. showNode(name)
      end

      if node.data.exps then
        ret = ret .. " = "

        for i, expr in ipairs(node.data.exps) do
          if i > 1 then
            ret = ret .. ", "
          end

          ret = ret .. showNode(expr)
        end
      end

      return ret
    elseif node.ty == "funcbody" then
      local ret = "function("

      for i, param in ipairs(node.data.pars) do
        if i > 1 then
          ret = ret .. ", "
        end

        ret = ret .. showNode(param)
      end

      ret = ret .. ")"

      for i, stat in ipairs(node.data.block) do
        ret = ret .. "\n" .. showNode(stat)
      end

      return ret .. "\nend"
    end
  end

  print(showNode(node))
end

function Parser:expected(what, slice, got, message)
  if got == "error" then
    return nil, message, slice
  end

  if Lexer.KEYWORDS[got] then
    return nil, ("%s expected, found keyword `%s`"):format(what, got), slice
  end

  if Lexer.OPS[3][got] then
    return nil, ("%s expected, found `%s`"):format(what, got), slice
  end

  if got == "number" then
    return nil, ("%s expected, found number literal"):format(what), slice
  end

  if got == "string" then
    return nil, ("%s expected, found string literal"):format(what), slice
  end

  if got == "EOF" then
    return nil, ("%s expected, found EOF"):format(what), slice
  end

  if got == "name" then
    return nil, ("%s expected, found identifier"):format(what), slice
  end

  return nil, ("%s expected"):format(what), slice
end

function Parser:unexpected(ty, node)
  return nil, ("unexpected %s"):format(ty), node.slice
end

function Parser:name()
  local startLine, startCol = self.l.line, self.l.col
  local ty, data, slice = self:peek()

  if ty and ty == "name" then
    self:next()
    return Parser.node(ty, data, slice)
  else
    return self:expected("name",
        slice or {startLine, startCol, startLine, startCol}, ty, data)
  end
end

function Parser:number()
  local startLine, startCol = self.l.line, self.l.col
  local ty, data, slice = self:peek()

  if ty and ty == "number" then
    self:next()
    return Parser.node(ty, data, slice)
  else
    return self:expected("number",
        slice or {startLine, startCol, startLine, startCol}, ty, data)
  end
end

function Parser:string()
  local startLine, startCol = self.l.line, self.l.col
  local ty, data, slice = self:peek()

  if ty and ty == "string" then
    self:next()
    return Parser.node(ty, data, slice)
  else
    return self:expected("string",
        slice or {startLine, startCol, startLine, startCol}, ty, data)
  end
end

function Parser:litntf()
  local startLine, startCol = self.l.line, self.l.col
  local ty, data, slice = self:peek()

  if ty and ty == "nil" then
    self:next()
    return Parser.node(ty, data, slice)
  elseif ty and (ty == "true" or ty == "false") then
    self:next()
    return Parser.node("boolean", ty == "true", slice)
  else
    return self:expected("expression",
        slice or {startLine, startCol, startLine, startCol}, ty, data)
  end
end

function Parser:expr()
  return self:l0()
end

function Parser:explist()
  local node, message, slice = self:expr()
  if not node then return nil, message, slice end

  local ret = {node}

  while true do
    if self:peek() == "," then
      self:next()
    else
      break
    end

    local node, message, slice = self:expr()
    if not node then return nil, message, slice end

    table.insert(ret, node)
  end

  return ret
end

function Parser:namelist()
  local node, message, slice = self:name()
  if not node then return nil, message, slice end

  local ret = {node}

  while true do
    if self:peek() == "," then
      self:save()
      self:next()
    else
      break
    end

    local node, message, slice = self:name()
    if not node then
      self:restore()
      return ret
    end

    self:commit()

    table.insert(ret, node)
  end

  return ret
end

function Parser:varlist()
  local node, message, slice = self:var()
  if not node then return nil, message, slice end

  local ret = {node}

  while true do
    if self:peek() == "," then
      self:next()
    else
      break
    end

    local node, message, slice = self:var()
    if not node then return nil, message, slice end

    table.insert(ret, node)
  end

  return ret
end

function Parser:table()
  local ty, message, slice = self:peek()
  if ty ~= "{" then
    return self:expected("`{`",
        slice or {startLine, startCol, startLine, startCol}, ty, message)
  end

  self:next()

  local startLine, startCol = slice[1], slice[2]
  local eLine, eCol = startLine, startCol

  local data = {map = {}, seq = {}}
  local first = true
  local last

  while true do
    if first and self:peek() == "}" then
      break
    end

    if self:peek() == "error" then
      local _, message, slice = self:peek()
      return nil, message, slice
    end

    if not first then
      local ty, _, slice = self:peek()
      if ty ~= "," and ty ~= ";" then
        break
      else
        self:next()
      end
    end

    if self:peek() == "}" then
      break
    end

    local key, value, message, slice, mustbe, named

    value, message, slice = self:expr()

    if self:peek() == "error" then
      local _, message, slice = self:peek()
      return nil, message, slice
    end

    if self:peek() == "[" then
      self:next()

      key, message, slice = self:expr()
      if not key then return nil, message, slice end

      local ty, message, slice = self:next()
      if ty ~= "]" then
        return self:expected("`]`",
          slice or {self.l.line, self.l.col, self.l.line, self.l.col}, ty, message)
      end
      mustbe = true
    elseif value then
      key = value
      value = nil
      named = true
    else
      return nil, message, slice
    end

    if mustbe or self:peek() == "=" then
      if named and key.ty ~= "name" then
        return self:expected("table index", key.slice, "expression")
      end

      if named then
        key.ty = "string"
      end

      local ty, message, slice = self:next()
      if ty ~= "=" then
        return self:expected("`=`",
            slice or {self.l.line, self.l.col, self.l.line, self.l.col}, ty, message)
      end

      value, message, slice = self:expr()
      if not value then return nil, message, slice end

      data.map[key] = value
      last = nil
    else
      value = key
      table.insert(data.seq, value)
      last = value
    end

    eLine, eCol = value.slice[3], value.slice[4]
    first = false
  end

  if self:peek() == "," or self:peek() == ";" then
    local slice = select(3, self:peek())
    eLine, eCol = slice[3], slice[4]

    self:next()
  end

  local ty, message, slice = self:next()
  if ty ~= "}" then
    return self:expected("`}`",
      slice or {eLine, eCol + 1, eLine, eCol + 1}, ty, message)
  end

  data.last = last

  return Parser.node("table", data, {startLine, startCol, slice[3], slice[4]})
end

function Parser:args()
  local startLine, startCol = self.l.line, self.l.col

  if self:peek() == "(" then
    self:next()

    if self:peek() == ")" then
      local slice = select(3, self:next())
      return {}, nil, {startLine, startCol, slice[3], slice[4]}
    end

    local args, message, slice = self:explist()
    if not args then return nil, message, slice end

    local last = args[#args].slice

    local ty, message, slice = self:next()
    if ty ~= ")" then
      return self:expected("`)`",
        slice or {last[3], last[4] + 1, last[3], last[4] + 1}, ty, message)
    end

    return args, nil, {startLine, startCol, slice[3], slice[4]}
  end

  local node, slice

  node, _, slice = self:string()

  if not node then
    node, _, slice = self:table()
  end

  if node then
    return {node}, nil, {startLine, startCol, node.slice[3], node.slice[4]}
  end

  return self:expected("function arguments",
      {startLine, startCol, startLine, startCol})
end

function Parser:prefixexp()
  local ret

  if self:peek() == "(" then
    local _, _, openSlice = self:next()

    local node, message, slice = self:expr()
    if not node then return nil, message, slice end

    local ty, message, slice = self:next()
    if ty ~= ")" then
      return self:expected("`)`",
        slice or {node.slice[3], node.slice[4] + 1, node.slice[3], node.slice[4] + 1}, ty, message)
    end

    ret = node
    ret.slice = {openSlice[1], openSlice[2], slice[3], slice[4]}
  else
    ret = self:name()
  end

  if not ret then
    return self:expected("expression", slice, select(1, self:peek()), select(2, self:peek()))
  end

  while true do
    local op = self:peek()

    if op == "." then
      self:next()

      local index, message, slice = self:name()
      if not index then return message, slice end

      ret = Parser.node("index", {indexing = ret, index = index, dot = true},
        {ret.slice[1], ret.slice[2], index.slice[3], index.slice[4]})
    elseif op == "[" then
      local message, slice

      self:next()

      local index, message, slice = self:expr()
      if not index then return nil, message, slice end

      local ty, message, slice = self:next()
      if ty ~= "]" then
        return self:expected("a closing square bracket",
          slice or {self.l.line, self.l.col, self.l.line, self.l.col}, ty, message)
      end

      ret = Parser.node("index", {indexing = ret, index = index, dot = false},
        {ret.slice[1], ret.slice[2], slice[3], slice[4]})
    elseif op == "(" or op == "{" or op == "string" then
      local args, message, slice = self:args()
      if not args then return nil, message, slice end

      ret = Parser.node("call", {callable = ret, args = args},
        {ret.slice[1], ret.slice[2], slice[3], slice[4]})
    elseif op == ":" then
      self:next()

      local index, message, slice = self:name()
      if not index then return message, slice end

      local args, message, slice = self:args()
      if not args then return nil, message, slice end

      ret = Parser.node("methodcall", {callable = ret, method = index, args = args},
        {ret.slice[1], ret.slice[2], slice[3], slice[4]})
    else
      break
    end
  end

  return ret
end

function Parser:var()
  local expr, message, slice = self:prefixexp()
  if not expr then return nil, message, slice end

  if expr.ty == "call" or expr.ty == "methodcall" then
    return nil, "function call is not a variable", expr.slice
  end

  return expr
end

function Parser:parlist()
  if self:peek() == "..." then
    local slice = select(3, self:peek())
    self:next()
    return {Parser.node("dots", nil, slice)}
  end

  local namelist, message, slice = self:namelist()
  if not namelist then return nil, message, slice end

  if self:peek() == "," then
    local slice = select(3, self:next())

    if self:peek() ~= "..." then
      return self:expected("expression or `...`",
          {slice[1], slice[2] + 1, slice[3], slice[4] + 1})
    end

    local slice = select(3, self:next())

    table.insert(namelist, Parser.node("dots", nil, slice))
  end

  return namelist
end

function Parser:funcbody(method, beginPosition, name)
  local ty, message, slice = self:peek()
  if ty ~= "(" then
    return self:expected("`(`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, ty, message)
  end

  self:next()

  local startLine, startCol = slice[1], slice[2]
  local endLine, endCol = startLine, startCol

  local parlist = self:parlist() or {}
  endLine, endCol = parlist[#parlist] and parlist[#parlist].slice[3] or endLine,
                    parlist[#parlist] and parlist[#parlist].slice[4] or endCol

  local ty, message, slice = self:next()
  if ty ~= ")" then
    return self:expected("`)`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, ty, message)
  end

  local topSlice = {startLine, startCol, slice[3], slice[4]}

  local block, message, slice = self:block()
  if not block then return nil, message, slice end

  endLine, endCol = block[#block] and block[#block].slice[3] or endLine,
                    block[#block] and block[#block].slice[4] or endCol

  local ty, message, slice = self:peek()
  if ty ~= "end" then
    return self:expected("`end`",
        slice or {endLine, endCol + 1, endLine, endCol + 1}, ty, message)
  end

  self:next()

  if method then
    table.insert(parlist, 1, Parser.node("name", "self"))
  end

  return Parser.node("funcbody", {pars = parlist, block = block,
                                  topSlice = topSlice, name = name,
                                  fullSlice={beginPosition[1],
                                             beginPosition[2],
                                             slice[3], slice[4]}},
      {startLine, startCol, slice[3], slice[4]})
end

function Parser:functiondef()
  local ty, message, slice = self:peek()
  if ty ~= "function" then
    return self:expected("`function`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, ty, message)
  end

  self:next()
  return self:funcbody(nil, {slice[1], slice[2]})
end

function Parser:term()
  local node, pmessage, pslice, message, slice

  if not self:peek() then
    local slice = self.previous and self.previous[3]
    return self:expected("expression",
        slice and {slice[3], slice[4] + 1, slice[3], slice[4] + 1}
               or {self.l.line, self.l.col, self.l.line, self.l.col}, "EOF")
  end

  if self:peek() == "error" then
    local t = {self:peek()}
    t[1] = nil
    return table.unpack(t)
  end

  local op = self:peek()

  if op == "number" then
    return self:number()
  elseif op == "nil" or op == "true" or op == "false" then
    return self:litntf()
  elseif op == "string" then
    return self:string()
  elseif op == "{" then
    return self:table()
  elseif op == "function" then
    return self:functiondef()
  elseif op == "..." then
    return Parser.node("dots", nil, select(3, self:next()))
  else
    return self:prefixexp()
  end
end

function Parser.lassoc(f, ...)
  local ops = hashset(...)

  return function(self)
    local startSlice = select(3, self:peek())

    local node, message, slice = f(self)
    if not node then
      return nil, message, slice
    end

    local ret = node

    while true do
      local op = self:peek()
      if ops[op] then
        self:next()
        local rhs, message, slice = f(self)
        if not rhs then
          return nil, message, slice
        end

        ret = Parser.node(
          "binop", {op = op, lhs = ret, rhs = rhs},
          {startSlice[1], startSlice[2], rhs.slice[3], rhs.slice[4]})
      else
        break
      end
    end

    return ret
  end
end

function Parser.rassoc(f, ...)
  local ops = hashset(...)

  local function rassoc(self)
    local node, message, slice = f(self)
    if not node then
      return node, message, slice
    end

    local op = self:peek()
    if not ops[op] then
      return node
    end

    self:next()

    local rhs = rassoc(self)

    if rhs then
      return Parser.node(
        "binop", {op = op, lhs = node, rhs = rhs},
        {node.slice[1], node.slice[2], rhs.slice[3], rhs.slice[4]})
    else
      return node
    end
  end

  return rassoc
end

function Parser.unop(f, ...)
  local ops = hashset(...)

  local function unop(self)
    local op, _, startSlice = self:peek()

    if ops[op] then
      self:next()
    else
      return f(self)
    end

    local node, message, slice = unop(self)
    if not node then return nil, message, slice end

    return Parser.node(
      "unop", {op = op, node = node},
      {startSlice[1], startSlice[2], node.slice[3], node.slice[4]})
  end

  return unop
end

Parser.l11 = Parser.rassoc(Parser.term, "^")
Parser.l10 = Parser.unop(Parser.l11, "not", "#", "-", "~")
Parser.l9  = Parser.lassoc(Parser.l10, "*", "/", "//", "%")
Parser.l8  = Parser.lassoc(Parser.l9, "+", "-")
Parser.l7  = Parser.rassoc(Parser.l8, "..")
Parser.l6  = Parser.lassoc(Parser.l7, "<<", ">>")
Parser.l5  = Parser.lassoc(Parser.l6, "&")
Parser.l4  = Parser.lassoc(Parser.l5, "~")
Parser.l3  = Parser.lassoc(Parser.l4, "|")
Parser.l2  = Parser.lassoc(Parser.l3, "<", ">", "<=", ">=", "~=", "==")
Parser.l1  = Parser.lassoc(Parser.l2, "and")
Parser.l0  = Parser.lassoc(Parser.l1, "or")

function Parser:block()
  local block = {}
  local stat, message, slice

  while true do
    stat, message, slice = self:stat()
    if message then return nil, message, slice end
    if not stat then break end

    table.insert(block, stat)
  end

  if self:peek() and self:peek() == "return" then
    table.insert(block, self:returnstat())
  end

  local n, message, slice = self:peek()
  if n and n ~= "end" and n ~= "until" and n ~= "else" and n ~= "elseif" then
    return self:expected("statement", slice, n, message)
  end

  return block, message, slice
end

function Parser:funcname()
  local ret, message, slice = self:name()
  if not ret then return nil, message, slice end

  local method

  while true do
    if self:peek() ~= "." then
      break
    end

    self:next()

    local name, message, slice = self:name()
    if not name then return nil, message, slice end

    name.ty = "string"

    ret = Parser.node("index", {indexing = ret, index = name},
        {ret.slice[1], ret.slice[2], name.slice[3], name.slice[4]})
  end

  if self:peek() == ":" then
    self:next()

    local name, message, slice = self:name()
    if not name then return nil, message, slice end

    name.ty = "string"

    ret = Parser.node("index", {indexing = ret, index = name},
        {ret.slice[1], ret.slice[2], name.slice[3], name.slice[4]})
    method = true
  end

  return ret, method
end

function Parser:stat()
  if self.savedstat then
    local ret = self.savedstat
    self.savedstat = nil
    return ret
  end

  local op, m, opslice = self:peek()

  if not op then
    return
  end

  if op == "error" then
    return nil, m, opslice
  end

  if op == "return" then return end

  if op == ";" then
    local slice = select(3, self:peek())
    self:next()
    return Parser.node("emptystat", nil, slice)
  elseif op == "do" then
    return self:dostat()
  elseif op == "while" then
    return self:whilestat()
  elseif op == "repeat" then
    return self:repeatstat()
  elseif op == "if" then
    return self:ifstat()
  elseif op == "goto" then
    return self:gotostat()
  elseif op == "::" then
    return self:labelstat()
  elseif op == "break" then
    local slice = select(3, self:peek())
    self:next()
    return Parser.node("break", nil, slice)
  elseif op == "for" then
    return self:forstat()
  end

  if op == "local" then
    self:next()

    local namelist = self:namelist()
    if namelist and self:peek() == "=" then
      self:next()

      local explist, message, slice = self:explist()
      if not explist then return nil, message, slice end

      return Parser.node("decl", {names = namelist, exps = explist},
          {opslice[1], opslice[2],
           explist[#explist].slice[3], explist[#explist].slice[4]})
    elseif namelist then
      return Parser.node("decl", {names = namelist},
          {opslice[1], opslice[2],
          namelist[#namelist].slice[3], namelist[#namelist].slice[4]})
    elseif self:peek() == "function" then
      self:next()

      local name, message, slice = self:name()
      if not name then return nil, message, slice end

      local body, message, slice = self:funcbody(
        nil, {opslice[1], opslice[2]}, name
      )
      if not body then return nil, message, slice end

      self.savedstat = Parser.node("assign",
          {vars = {name}, exps = {body}, func = true},
          {opslice[1], opslice[2], body.slice[3], body.slice[4]})

      return Parser.node("decl", {names = {name}},
          {opslice[1], opslice[2], name.slice[3], name.slice[4]})
    end
  end

  if op == "function" then
    self:next()

    local name, message, slice = self:funcname()
    if not name then return nil, message, slice end

    local body, message, slice = self:funcbody(
      message, {opslice[1], opslice[2]}, name
    )
    if not body then return nil, message, slice end

    return Parser.node("assign", {vars = {name}, exps = {body}, func = true},
        {opslice[1], opslice[2], body.slice[3], body.slice[4]})
  end

  local prefixexp, message, slice = self:prefixexp()
  if prefixexp and (prefixexp.ty == "methodcall" or prefixexp.ty == "call") then
    return prefixexp
  end

  local varlist = {}

  if prefixexp then
    table.insert(varlist, prefixexp)
  end

  if self:peek() == "," then
    self:next()
    local others, message, slice = self:varlist()
    if not others then return nil, message, slice end

    for _, v in ipairs(others) do
      table.insert(varlist, v)
    end
  end

  if #varlist > 0 then
    local ty, message, slice = self:next()
    if ty ~= "=" then
      return self:expected("`=`",
          slice or {varlist[#varlist].slice[3], varlist[#varlist].slice[4] + 1,
                    varlist[#varlist].slice[3], varlist[#varlist].slice[4] + 1}, ty, message)
    end

    local explist, message, slice = self:explist()
    if not explist then return nil, message, slice end

    return Parser.node("assign", {vars = varlist, exps = explist},
        {varlist[1].slice[1], varlist[1].slice[2],
         explist[#explist].slice[3], explist[#explist].slice[4]})
  end
end

function Parser:dostat()
  local op, message, opslice = self:next()
  if op ~= "do" then
    return self:expected("`do`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, op, message)
  end

  local block, message, slice = self:block()
  if not block then return nil, message, slice end

  local ty, message, slice = self:next()
  if ty ~= "end" then
    return self:expected("`end`",
        slice or {block[#block].slice[3], block[#block].slice[4] + 1,
                  block[#block].slice[3], block[#block].slice[4] + 1}, ty, message)
  end

  return Parser.node("do", block,
      {opslice[1], opslice[2], slice[3], slice[4]})
end

function Parser:whilestat()
  local op, message, opslice = self:next()
  if op ~= "while" then
    return self:expected("`while`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, op, message)
  end

  local cond, message, slice = self:expr()
  if not cond then return nil, message, slice end

  local op, message, slice = self:next()
  if op ~= "do" then
    return self:expected("`do`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, op, message)
  end

  local topSlice = {opslice[1], opslice[2], slice[3], slice[4]}

  local block, message, slice = self:block()
  if not block then return nil, message, slice end

  local ty, message, slice = self:next()
  if ty ~= "end" then
    return self:expected("`end`",
        slice or {block[#block].slice[3], block[#block].slice[4] + 1,
                  block[#block].slice[3], block[#block].slice[4] + 1}, ty, message)
  end

  return Parser.node("while", {cond = cond, block = block, topSlice = topSlice},
      {opslice[1], opslice[2], slice[3], slice[4]})
end

function Parser:repeatstat()
  local op, message, opslice = self:next()
  if op ~= "repeat" then
    return self:expected("`repeat`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, op, message)
  end

  local block, message, slice = self:block()
  if not block then return nil, message, slice end

  local ty, message, slice = self:next()
  if ty ~= "until" then
    return self:expected("`until`",
        slice or {block[#block].slice[3], block[#block].slice[4] + 1,
                  block[#block].slice[3], block[#block].slice[4] + 1}, ty, message)
  end

  local bottomSlice = {slice[1], slice[2]}

  local cond, message, slice = self:expr()
  if not cond then return nil, message, slice end

  bottomSlice[3] = slice[3]
  bottomSlice[4] = slice[4]

  return Parser.node("repeat", {cond = cond, block = block,
                                bottomSlice = bottomSlice},
      {opslice[1], opslice[2], cond.slice[3], cond.slice[4]})
end

function Parser:ifstat()
  local endLine, endCol

  local op, message, opslice = self:next()
  if op ~= "if" then
    return self:expected("`if`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, op, message)
  end

  local ifCond, message, slice = self:expr()
  if not ifCond then return nil, message, slice end

  local ty, message, slice = self:next()
  if ty ~= "then" then
    return self:expected("`then`",
        slice or {ifCond.slice[3], ifCond.slice[4] + 1,
                  ifCond.slice[3], ifCond.slice[4] + 1}, ty, message)
  end

  local condSlice = {opslice[1], opslice[2], slice[3], slice[4]}

  endLine, endCol = slice[3], slice[4]

  local block, message, slice = self:block()
  if not block then return nil, message, slice end

  local pelseif = {}
  local pelse
  local elseSlice

  if #block > 0 then
    endLine, endCol = block[#block].slice[3], block[#block].slice[4]
  end

  while true do
    local op, _, opslice = self:peek()
    if op == "elseif" then
      self:next()
    else
      break
    end

    local cond, message, slice = self:expr()
    if not cond then return nil, message, slice end

    local branchSlice = {opslice[1], opslice[2]}

    local ty, message, slice = self:next()
    if ty ~= "then" then
      return self:expected("`then`",
          slice or {cond.slice[3], cond.slice[4] + 1,
                    cond.slice[3], cond.slice[4] + 1}, ty, message)
    end

    endLine, endCol = slice[3], slice[4]
    branchSlice[3] = slice[3]
    branchSlice[4] = slice[4]

    local block, message, slice = self:block()
    if not block then return nil, message, slice end

    if #block > 0 then
      endLine, endCol = block[#block].slice[3], block[#block].slice[4]
    end

    table.insert(pelseif, {cond = cond, block = block, slice = branchSlice})
  end

  if self:peek() == "else" then
    local slice = select(3, self:peek())

    endLine, endCol = slice[3], slice[4]
    elseSlice = slice

    self:next()
    pelse, message, slice = self:block()
    if not pelse then return nil, message, slice end

    if #pelse > 0 then
      endLine, endCol = pelse[#pelse].slice[3], pelse[#pelse].slice[4]
    end
  end

  local ty, message, slice = self:next()
  if ty ~= "end" then
    return self:expected("`end`",
        slice or {endLine, endCol + 1,
                  endLine, endCol + 1}, ty, message)
  end

  return Parser.node("if", {
    cond = ifCond,
    condSlice = condSlice,
    block = block,
    pelseif = pelseif,
    pelse = pelse,
    elseSlice = elseSlice
  }, {opslice[1], opslice[2], slice[3], slice[4]})
end

function Parser:gotostat()
  local op, message, opslice = self:next()
  if op ~= "goto" then
    return self:expected("`goto`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, op, message)
  end

  local label, message, slice = self:name()
  if not label then return nil, message, slice end

  return Parser.node("goto", label,
      {opslice[1], opslice[2], label.slice[3], label.slice[4]})
end

function Parser:labelstat()
  local op, message, opslice = self:next()
  if op ~= "::" then
    return self:expected("`::`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, op, message)
  end

  local label, message, slice = self:name()
  if not label then return nil, message, slice end

  local ty, message, slice = self:next()
  if ty ~= "::" then
    return self:expected("`::`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, op, message)
  end

  return Parser.node("label", label,
      {opslice[1], opslice[2], slice[3], slice[4]})
end

function Parser:returnstat()
  local op, message, opslice = self:peek()
  if op ~= "return" then
    return self:expected("`return`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, op, message)
  end

  self:next()

  local values = self:explist()
  if not values then
    values = {}
  end

  local slice = values[#values] and values[#values].slice or opslice

  return Parser.node("return", values,
      {opslice[1], opslice[2], slice[3], slice[4]})
end

function Parser:forstat()
  local op, message, opslice = self:next()
  if op ~= "for" then
    return self:expected("`for`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, op, message)
  end

  local counter, message, slice = self:name()
  if not counter then return nil, message, slice end

  local init, limit, stop, names, exps

  if self:peek() == "=" then
    local slice = select(3, self:peek())

    self:next()

    init, limit, step, other = table.unpack(self:explist())
    if not init then
      return self:expected("expression (for-loop initializer)",
          {slice[3], slice[4] + 1, slice[3], slice[4] + 1})
    end

    if not limit then
      return self:expected("expression (for-loop limit)",
          {slice[3], slice[4] + 1, slice[3], slice[4] + 1})
    end

    if other then
      return self:unexpected("expression", other)
    end
  else
    local message, slice
    names = {}

    if self:peek() == "," then
      self:next()
      names, message, slice = self:namelist()
      if not names then return nil, message, slice end
    end

    table.insert(names, 1, counter)

    local ty, message, slice = self:next()
    if ty ~= "in" then
      return self:expected("`in`",
          slice or {self.l.line, self.l.col, self.l.line, self.l.col}, ty, message)
    end

    exps, message, slice = self:explist()
    if not exps then return nil, message, slice end

    counter = nil
  end

  local ty, message, slice = self:next()
  if ty ~= "do" then
    return self:expected("`do`",
        slice or {self.l.line, self.l.col, self.l.line, self.l.col}, ty, message)
  end

  local topSlice = {opslice[1], opslice[2], slice[3], slice[4]}

  local block, message, slice = self:block()
  if not block then return nil, message, slice end

  local ty, message, slice = self:next()
  if ty ~= "end" then
    return self:expected("`end`",
        slice or {block[#block].slice[3], block[#block].slice[4] + 1,
                  block[#block].slice[3], block[#block].slice[4] + 1}, ty, message)
  end

  return Parser.node("for", {
    counter = counter, names = names, exps = exps,
    block = block, init = init, limit = limit, step = step,
    topSlice = topSlice
  }, {opslice[1], opslice[2], slice[3], slice[4]})
end

return {
  Lexer = Lexer,
  Parser = Parser
}
