# crateriform
# Copyright (C) 2019 Fingercomp
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gc
import os.path
import sys
import time

from dataclasses import dataclass
from math import floor, ceil
from typing import List, Optional, Tuple, Union

import pygame
import pygame.display
import pygame.font
import pygame.image
import pygame.surfarray
import pygments
import pygments.lexer
import pygments.lexers
import pygments.styles
import pygments.token
import numpy as np

from pygame import Color, Rect, Surface
from pygame.image import save as save_image
from pygame.font import SysFont


class InterpretedLuaLexer(pygments.lexers.LuaLexer):
    """A Lua lexer modified to support the tokens added by the interpreter.

    Specifically, this class recognizes the function, thread and userdata tokens
    as literals:

    - ``<thread: 0x123456789012>``
    - ``<main thread>``
    - ``<coroutine #1>``
    - ``<userdata>``
    - ``…``
    - ``<function(args, ...)>``
    - ``<math.tointeger(...)>``
    - ``<Parser:new(parser):13>``
    """

    tokens = {
        'base': [
            (r'(<thread: 0x[0-9a-f]+>)', pygments.token.Literal),
            (r'(<main thread>)', pygments.token.Literal),
            (r'(<coroutine #\d+>)', pygments.token.Literal),
            (r'(<userdata>)', pygments.token.Literal),
            ('(…)', pygments.token.Punctuation),
            (r'(\u00a0= .\+?\u00a0)', pygments.token.Literal),
            (r'(<[^(]+?\([^)]*?\)(?::[0-9]+|)>)', pygments.token.Literal),
            pygments.lexer.inherit
        ]
    }


RelCoordinate = Tuple[int, int]
AbsCoordinate = Tuple[int, int]
RelSpan = Tuple[RelCoordinate, RelCoordinate]
AbsSpan = Tuple[AbsCoordinate, AbsCoordinate]


def cmp_coords(p1, p2):
    if p1[0] < p2[0] or p1[0] == p2[0] and p1[1] < p2[1]:
        return -1
    elif p2[0] < p1[0] or p2[0] == p1[0] and p2[1] < p1[1]:
        return 1
    else:
        return 0


def cmp_range(begin, end, p):
    if cmp_coords(p, begin) < 0:
        return -1
    elif cmp_coords(p, end) > 0:
        return 1
    else:
        return 0


class TextBuffer:

    #       y
    #   +------->
    #   |123456         (2, 3) == "9"
    # x |789012         (3, 6) == "8"
    #   |345678         (1, 2) == "2"
    #   v

    def __init__(self, text):
        self.lines = text.copy()
        self.line_offsets = []

    def map(self, p, require_distinct=False):
        x = p[0]
        y = p[1]

        for begin, end, distinct in reversed(self.line_offsets):
            if (begin[0] == x and (begin[1] == y and
                                   not (distinct and require_distinct) or
                                   begin[1] < y)):
                x = end[0]
                y = end[1] + y - begin[1]
                break

            if begin[0] < x and begin[0] != end[0]:
                # there are shifted lines above
                x = end[0] + x - begin[0]
                break

        if __debug__:
            print('Mapped {!r} to {!r})'.format(p, (x, y)))

        return (x, y)

    def map_range(self, begin, end):
        return (self.map(begin, True), self.map(end))

    def add_mapping(self, begin, end, new_coord):
        distinct_begin_end = cmp_coords(begin, end) == 0
        mapped_begin, mapped_end = self.map_range(begin, end)

        if __debug__:
            print('Mapping [{!r}->{!r}, {!r}->{!r}] to {!r} ({!s})'.format(
                begin, mapped_begin,
                end, mapped_end,
                new_coord,
                distinct_begin_end
            ))

        self.line_offsets = [x for x in self.line_offsets
                             if cmp_range(begin, end, x[0]) != 0]

        if cmp_coords(self.map(end), new_coord) != 0:
            # Does not map to itself

            # Insert in sorted order
            for i in range(len(self.line_offsets) + 1):
                if ((i == 0 or
                        cmp_coords(self.line_offsets[i - 1][0], end) < 0) and
                        (i == len(self.line_offsets) or
                         cmp_coords(end, self.line_offsets[i][0]) < 0)):
                    self.line_offsets.insert(i, (end, new_coord,
                                                 distinct_begin_end))
                    break
        else:
            i = -1

        # Shift following mappings
        for j in range(i + 1, len(self.line_offsets)):
            b, e, d = self.line_offsets[j]

            if __debug__:
                print(b, e, d)

            if mapped_end[0] == e[0] and mapped_end[1] <= e[1]:
                self.line_offsets[j] = (b,
                                        (new_coord[0],
                                         e[1] + new_coord[1] - mapped_end[1]),
                                        d)
            elif mapped_end[0] != new_coord[0] and mapped_end[0] < e[0]:
                self.line_offsets[j] = (b,
                                        (e[0] + new_coord[0] - mapped_end[0],
                                         e[1]),
                                        d)

    def __getitem__(self, s):
        if isinstance(s, tuple):
            x, y = self.map(s)
            return self.lines[x][y]

        if not isinstance(s, slice):
            raise TypeError('key must be either slice or tuple')

        if s.step is not None:
            raise ValueError('slice with non-1 step is not supported')

        begin, end = self.map_range(s.start, s.stop)

        comparison = cmp_coords(begin, end)

        if comparison == 0:
            return [self.lines[begin[0] - 1][begin[1] - 1]]
        elif comparison > 0:
            raise ValueError('begin {!r} > end {!r}'.format(begin, end))
        elif begin[0] == end[0]:
            # inclusive on both ends
            return [self.lines[end[0] - 1][begin[1] - 1 : end[1]]]
        else:
            lines = [self.lines[begin[0] - 1][begin[1] - 1:]]

            for i in range(begin[0], end[0] - 1):
                lines.append(self.lines[i])

            lines.append(self.lines[end[0] - 1][:end[1]])

            return lines

    def __setitem__(self, s, v):
        if isinstance(s, tuple):
            s = slice(s, s, None)

        if not isinstance(s, slice):
            raise TypeError('key must be either slice or tuple')

        if s.step is not None:
            raise ValueError('slice with non-1 step is not supported')

        if not isinstance(v, str):
            raise TypeError('value must be string')

        start = s.start
        stop = s.stop

        begin, end = self.map_range(start, stop)

        if cmp_coords(begin, end) > 0:
            raise ValueError('begin {!r} > end {!r}'.format(begin, end))

        if v == '':
            # must replace, not remove
            # otherwise, mapping goes wild
            # try to shift end one character to right,
            # adding space at end of line if necessary
            if end[1] == len(self.lines[end[0] - 1]):
                v = ' '
            else:
                end = (end[0], end[1] + 1)
                stop = (stop[0], stop[1] + 1)
                v = self.lines[end[0] - 1][end[1] - 1]

        lines = v.split('\n')

        replacement_len = (len(lines), len(lines[-1]))

        if replacement_len[0] == 1:
            end_mapping = (begin[0], begin[1] + replacement_len[1] - 1)
        else:
            end_mapping = (begin[0] + replacement_len[0] - 1,
                           replacement_len[1])

        self.add_mapping(start, stop, end_mapping)

        # cases 1, 5
        if len(lines) == 1:
            self.lines[begin[0] - 1] = (self.lines[begin[0] - 1] \
                                            [:begin[1] - 1] +
                                        lines[0] +
                                        self.lines[end[0] - 1][end[1]:])
            self.lines[begin[0] : end[0]] = []
            return

        # cases 2, 3, 4, 6
        if replacement_len[0] > 1:
            last_line = lines[-1] + self.lines[end[0] - 1][end[1]:]

            if begin[0] == end[0]:
                self.lines.insert(begin[0], last_line)
            else:
                self.lines[end[0] - 1] = last_line

            self.lines[begin[0] : end[0] - 1] = []

            for i in reversed(range(1, replacement_len[0] - 1)):
                self.lines.insert(begin[0], lines[i])

            self.lines[begin[0] - 1] = (
                self.lines[begin[0] - 1][:begin[1] - 1] +
                lines[0]
            )
            return


def partition_line(line_number, line, default, parts):
    result = []
    last_level = None
    begin = 1
    end = 0

    def get_level(i, layer):
        for level, (begin, end) in reversed(parts[layer]):
            if cmp_range(begin, end, (line_number, i)) == 0:
                return level
        else:
            return default[layer]

    for i, c in enumerate(line, start=1):
        level = tuple(get_level(i, x) for x in range(len(parts)))

        if last_level is None or level == last_level:
            end = i
            last_level = last_level or level
        else:
            result.append((last_level, ((line_number, begin),
                                        (line_number, end))))
            last_level = level
            begin = end = i

    result.append((last_level or default, ((line_number, begin),
                                           (line_number, end))))

    return result


def crop_lines(limit, lines, required_spans):
    required_spans = list(filter(None, required_spans))
    begin = None
    end = None

    for b, e in required_spans:
        if begin is None or b[0] < begin:
            begin = b[0]
        if end is None or e[0] > end:
            end = e[0]

    if begin is None and end is None:
        return enumerate(lines[:limit], start=1), (1, len(lines))

    delta = limit - (end - begin)

    while delta > 0:
        if begin > 1 and (delta % 2 == 1 or end == len(lines)):
            begin -= 1
        elif end < len(lines) and (delta % 2 == 0 or begin == 1):
            end += 1
        else:
            break

        delta -= 1

    return enumerate(lines[begin - 1 : end], start=begin), (begin, end)


def lex(lines):
    result = []
    (x, y) = (1, 1)

    for ttype, token in pygments.lex(b'\n'.join(x.encode('utf-8')
                                                for x in lines),
                                     InterpretedLuaLexer()):
        for n, line in enumerate(token.split('\n')):
            if n == 0:
                begin = (x, y)
                end = (x, y + len(line) - 1)
            else:
                begin = (x + n, 1)
                end = (x + n, len(line))

            if len(line) > 0:
                result.append((ttype, (begin, end)))

        (x, y) = end
        y += 1

    return result


class BaseRenderer:
    @dataclass
    class Buffer:
        buffer: TextBuffer
        lexed: Optional[List[Tuple[RelSpan, pygments.token._TokenType]]] = None
        highlight: Optional[RelSpan] = None
        value: Optional[Tuple[RelSpan, RelSpan]] = None
        call: Optional[RelSpan] = None

    COLORS = {k: Color('#' + v['color'])
              for k, v in pygments.styles.get_style_by_name('colorful')
              if v['color']}

    COLORS.update(
        {'line': (Color('#333333'), Color('#f0f0f0')),
         'highlighted_line': (Color('#121212'), Color('#e0e0e0')),
         'highlighted_expr': (Color('#ffffff'), Color('#606060')),
         'value': (Color('#ffffff'), Color('#333333'))}
    )

    def __init__(self):
        self.current_coroutine = 0
        self.coroutines = {}

    def get_coroutine(self):
        return self.coroutines[self.current_coroutine]

    def set_coroutine(self, num):
        self.current_coroutine = num

    def add_coroutine(self, num):
        self.coroutines[num] = []
        self.current_coroutine = num

    def del_coroutine(self, num):
        del self.coroutines[num]

    def add_buffer(self, buf):
        self.get_coroutine().append(self.Buffer(buffer=buf))

    def pop_buffer(self):
        return self.get_coroutine().pop()

    def get_buffer(self):
        try:
            return self.get_coroutine()[-1]
        except IndexError:
            return None

    def render(self):
        rendered_coroutines = {}

        for i, co in self.coroutines.items():
            dimensions, rendered_buffers = self._render_buffer(
                0, co, 0
            )

            rendered_coroutines[i] = self._render_coroutine(dimensions,
                                                            rendered)

        return self._render(rendered_coroutines)

    def _render_buffer(self, buf_idx, co, y, rendered_stack=None):
        if rendered_stack is None:
            rendered_stack = [None]
        else:
            rendered_stack.append(None)

        stack_position = len(rendered_stack) - 1

        buf = co[buf_idx]
        highlight = None

        if buf.highlight:
            highlight = buf.buffer.map_range(*buf.highlight)

        if not buf.lexed:
            buf.lexed = lex(buf.buffer.lines)

        rendered_lines = []
        w = 0
        h = 0

        highlight_params = tuple(x for x in (
            ('highlighted_line', highlight),
            ('highlighted_expr', buf.value[0] if buf.value else None),
            ('value', buf.value[1] if buf.value else None),
            ('call', buf.call),
        ) if x[1])

        # if buf_idx + 1 == len(co):
        #     iterator = enumerate(buf.buffer.lines, start=1)
        #     line_range = (1, len(buf.buffer.lines))
        # else:
        iterator, line_range = crop_lines(10, buf.buffer.lines,
                                          [x[1] for x in highlight_params])

        for n, line in iterator:
            parts = partition_line(n, line, ('line', pygments.token.Text),
                                   (highlight_params, buf.lexed))

            line_width = 0
            rendered_parts = []

            for i, (level, (begin, end)) in enumerate(parts):
                if level[0] == 'call':
                    level = ('highlighted_expr', *level[1:])

                level = tuple(level)

                if level in BaseRenderer.COLORS:
                    ch_color, bg_color = BaseRenderer.COLORS[level]
                else:
                    ch_color, bg_color = BaseRenderer.COLORS[level[0]]

                    if (level[1] in BaseRenderer.COLORS and
                            level[0] not in ['highlighted_expr', 'value']):
                        ch_color = BaseRenderer.COLORS[level[1]]

                s = line[begin[1] - 1 : end[1]]

                if not s:
                    bg_color = None

                rendered_part, part_width = self._compile_line_part(
                    s,
                    ch_color,
                    bg_color
                )
                rendered_parts.append((rendered_part, (line_width, 0)))
                line_width += part_width

            rendered_line, line_height = self._compile_line(
                line_width, rendered_parts
            )

            rendered_lines.append((rendered_line, (0, h)))
            h += line_height
            w = max(w, line_width)

            if buf.call and buf.call[1][0] == n and buf_idx != len(co) - 1:
                ((sub_w, sub_h), _) = self._render_buffer(
                    buf_idx + 1,
                    co,
                    y + h,
                    rendered_stack
                )
                # w = max(w, sub_w)
                h += sub_h

        rendered_buffer = self._compile_buffer((w, h), rendered_lines)
        rendered_stack[stack_position] = (rendered_buffer, (0, y), line_range)

        return ((w, h), rendered_stack)

    def _compile_line_part(self, string, parts, level, ch_color, bg_color):
        raise NotImplementedError

    def _compile_line(self, line_width, parts):
        raise NotImplementedError

    def _compile_buffer(self, dimensions, rendered_lines):
        raise NotImplementedError

    def _render_coroutine(self, dimensions, rendered_buffers):
        raise NotImplementedError

    def _render(self, rendered_coroutines):
        raise NotImplementedError


class ImageRenderer(BaseRenderer):
    @dataclass
    class CacheEntry:
        shadows: List[Tuple[Rect, float, bool]]
        surface: Optional[Surface] = None
        top_buffer_y: Optional[int] = None
        top_buffer_h: Optional[int] = None
        number_label: Optional[Surface] = None

    def __init__(self, font_name, size):
        super().__init__()
        self.font = SysFont(font_name, size)
        self._cache = {}

    def add_buffer(self, buf):
        self.get_cache().surface = None

        return super().add_buffer(buf)

    def pop_buffer(self):
        self.get_cache().surface = None

        return super().pop_buffer()

    def add_coroutine(self, num):
        super().add_coroutine(num)
        self._cache[num] = self.CacheEntry(shadows=[])

    def del_coroutine(self, num):
        super().del_coroutine(num)
        del self._cache[num]

    def render(self):
        rendered_coroutines = {}

        for i in sorted(self.coroutines.keys()):
            co = self.coroutines[i]
            if self.get_cache(i).surface and len(co) > 1:
#                if i == self.current_coroutine:
                (dimensions, top_buffer) = self._render_buffer(
                    len(co) - 1,
                    co,
                    self.get_cache(i).top_buffer_y
                )

                top_buffer = top_buffer[0][0]

                rendered_coroutines[i] = (
                    self._cached_render_coroutine(dimensions, top_buffer, i)
                )
#                else:
#                    surface = self.get_cache(i).surface.copy()
#                    self._draw_shadows(surface, i)
#
#                    rendered_coroutines.append(surface)
            else:
                dimensions, rendered_buffers = self._render_buffer(
                    0, co, 0
                )

                rendered_coroutines[i] = (
                    self._render_coroutine(dimensions, rendered_buffers, i)
                )

        return self._render(rendered_coroutines)

    def cast_shadow(self, surface, rect, opacity=1, top_to_bottom=False):
        height = rect.height

        rect.top = max(0, rect.top)
        rect.bottom = min(surface.get_height(), rect.bottom)

        if top_to_bottom:
            iterator = range(rect.top, rect.bottom)
        else:
            iterator = range(rect.bottom, rect.top, -1)

        sarray = pygame.surfarray.pixels3d(surface)

        for i, y in enumerate(iterator, start=1):
            alpha = 1 - (1 - i / height) * opacity
            s = np.multiply(sarray[:, y - 1, :], alpha)
            np.floor(s, out=s)
            sarray[:, y - 1, :] = s

        del sarray

        return surface

    def get_cache(self, co=None):
        if co is None:
            co = self.current_coroutine

        return self._cache[co]

    def _compile_line_part(self, string, ch_color, bg_color):
        part = self.font.render(string, True, ch_color, bg_color)

        return part, part.get_width()

    def _compile_line(self, line_width, parts):
        rendered_line = Surface((line_width, parts[0][0].get_height()))
        rendered_line.blits(parts, False)

        return rendered_line, rendered_line.get_height()

    def _compile_buffer(self, dimensions, rendered_lines):
        surface = Surface(dimensions, flags=pygame.HWSURFACE)
        surface.fill((0x00, 0xff, 0x00))
        surface.set_colorkey((0x00, 0xff, 0x00))
        surface.blits([x for x in rendered_lines
                       if x[0].get_width() != 1], False)

        return surface

    def _draw_shadows(self, surface, co):
        for shadow in self.get_cache(co).shadows:
            if Rect((0, 0), surface.get_size()).contains(shadow[0]):
                self.cast_shadow(surface, *shadow)

    def _render_coroutine(self, dimensions, rendered_buffers, co):
        top_buffer = rendered_buffers.pop()

        w = max((buf[0].get_width() for buf in rendered_buffers),
                default=1)

        surface = Surface((w, dimensions[1]), flags=pygame.HWSURFACE)
        surface.set_colorkey((0x00, 0xff, 0x00))

        surface.fill(BaseRenderer.COLORS['line'][1])
        surface.blits(tuple((x[0], x[1]) for x in rendered_buffers), False)

        shadow_height = 6
        opacity = 0.2

        # copy without shadows
        cache_entry = self.get_cache(co)
        cache_entry.surface = surface
        cache_entry.top_buffer_y = top_buffer[1][1]
        cache_entry.top_buffer_h = top_buffer[0].get_height()
        cache_entry.shadows.clear()

        for i, (buf, pos, lines) in enumerate(rendered_buffers):
            if i > 0:
                cache_entry.shadows.extend((
                    (Rect(0, pos[1] - shadow_height, w, shadow_height),
                     opacity,
                     False),
                    (Rect(0, pos[1] + buf.get_height(),
                          w, shadow_height),
                     opacity,
                     True)
                ))

        buf, pos, lines = top_buffer
        cache_entry.shadows.extend((
            (Rect(0, pos[1] - shadow_height, w, shadow_height),
             opacity,
             False),
            (Rect(0, pos[1] + buf.get_height(),
                  w, shadow_height),
             opacity,
             True)
        ))

        del rendered_buffers

        return self._cached_render_coroutine(buf.get_size(),
                                             buf,
                                             co)

    def _cached_render_coroutine(self, dimensions, top_buffer, co):
        cache_entry = self.get_cache(co)
        surface = cache_entry.surface
        height_delta = top_buffer.get_height() - cache_entry.top_buffer_h

        if dimensions[0] > surface.get_width() or height_delta != 0:
            new_dimensions = (max(dimensions[0], surface.get_width()),
                              surface.get_height() + height_delta)
            new_surface = Surface(new_dimensions,
                                  flags=pygame.HWSURFACE)
            new_surface.set_colorkey(Color('#00ff00'))
            new_surface.fill(BaseRenderer.COLORS['line'][1])

            if height_delta == 0:
                new_surface.blit(surface, (0, 0))
            else:
                new_surface.blit(surface, (0, 0),
                                 Rect((0, 0), (surface.get_width(),
                                               cache_entry.top_buffer_y)))

                dest_top = cache_entry.top_buffer_y + top_buffer.get_height()
                src_area = Rect(0, 0, surface.get_width(), 0)
                src_area.top = (cache_entry.top_buffer_y +
                                cache_entry.top_buffer_h)
                src_area.height = surface.get_height() - src_area.top

                new_surface.blit(surface, (0, dest_top), src_area)

            del surface

            surface = new_surface
        else:
            surface = surface.copy()

        surface.fill(BaseRenderer.COLORS['line'][1],
                     Rect(0, cache_entry.top_buffer_y,
                          surface.get_width(), dimensions[1]))
        surface.blit(top_buffer, (0, cache_entry.top_buffer_y))

        for i, shadow in enumerate(cache_entry.shadows):
            if i % 2 == 1:
                shadow[0].top += height_delta

        self._draw_shadows(surface, co)

        for i, shadow in enumerate(cache_entry.shadows):
            if i % 2 == 1:
                shadow[0].top -= height_delta

        return surface

    def _render(self, rendered_coroutines):
        separator_thickness = 10
        separator_color = Color('#282828')
        ch_color = Color('#dddddd')

        w = sum(x.get_width() for x in rendered_coroutines.values())
        w += separator_thickness * (len(rendered_coroutines) + 1)

        h = max(x.get_height() for x in rendered_coroutines.values())
        h += 3 * separator_thickness

        surface = Surface((w, h))
        surface.set_colorkey(Color('#00ff00'))
        surface.fill(separator_color)

        x = separator_thickness
        y = separator_thickness * 2

        overlay = Surface((w, h))
        overlay.set_alpha(32)

        blits = []

        for i in sorted(rendered_coroutines.keys()):
            co = rendered_coroutines[i]
            blits.append((co, (x, y)))

            if i != self.current_coroutine:
                blits.append((overlay, (x, y), Rect((x, y), co.get_size())))

            label = self.get_cache(i).number_label

            if label is None:
                if i == 0:
                    text = 'Main thread'
                else:
                    text = 'Coroutine #{}'.format(i)

                label = self.font.render(text, True, ch_color, separator_color)
                self.get_cache(i).number_label = label

            blits.append((label, (x, (y - label.get_height()) // 2)))

            x += separator_thickness + co.get_width()

        surface.blits(blits)

        return surface


def parse_slice(p1, p2):
    p1 = p1.split(',')
    p2 = p2.split(',')
    p1 = (int(p1[0]), int(p1[1]))
    p2 = (int(p2[0]), int(p2[1]))
    return (p1, p2)


def count_leading_spaces(s):
    if not s:
        return 0

    result = 0
    for c in s:
        if c != ' ':
            break

        result += 1

    return result


if __name__ == '__main__':
    output = 'file'
    delay = None

    if len(sys.argv) < 3:
        print('Not enough arguments.')
        print('Usage: visual.py <code file> <event file> [output] [delay]')
        print('  <code file>   path to interpreted program code')
        print('  <event file>  path to event file')
        print("  [output]      'tty', 'tty-no-color', 'display', 'debug'")
        print('                or path to directory to save frames to')
        print("                default: 'display'")
        print('  [delay]       delay between successive frames')
        print('                if omitted, next frame is shown by pressing')
        print('                [Enter] key')
        sys.exit(1)
    elif len(sys.argv) == 3:
        output = 'display'
    elif sys.argv[3] not in ['tty', 'tty-no-color', 'display', 'debug']:
        output = 'file'
        output_file = sys.argv[3]
    else:
        output = sys.argv[3]

    if len(sys.argv) > 4:
        delay = float(sys.argv[4])

    if output == 'display':
        pygame.display.init()

    pygame.font.init()

    renderer = ImageRenderer('Ubuntu Mono', 12)

    if output == 'display':
        surface = pygame.display.set_mode((800, 800))

    def buf():
        return renderer.get_buffer().buffer

    with open(sys.argv[1], 'r') as f:
        original_code = f.read().splitlines()
        renderer.add_coroutine(0)
        renderer.add_buffer(TextBuffer(original_code))

    with open(sys.argv[2], 'r') as f:
        marks = f.read().splitlines()

    for i, v in enumerate(marks):
        (p1, p2, ty, *val) = v.split(' ', 3)
        (p1, p2) = parse_slice(p1, p2)

        if ty in ['expr', 'eval', 'call', 'makecall', 'co', 'newco']:
            val = val[0]
        else:
            val = None

        marks[i] = (p1, p2, ty, val)

    def replace(begin, end, value):
        if output not in ['tty', 'tty-no-color']:
            print('Replacing [{!r}, {!r}] with {!r}'.format(begin, end, value))

        buf()[begin : end] = value
        renderer.get_buffer().lexed = None

        if __debug__:
            if output not in ['tty', 'tty-no-color']:
                print('\n'.join('{: >3d} │ {}'.format(l + 1, x)
                            for l, x in enumerate(buf().lines)))
                print(buf().line_offsets)
                print()

    def updater():
        for begin, end, ty, value in marks:
            print(f'> {begin}-{end}: {ty} {value}')

            if ty == 'expr':
                renderer.get_buffer().call = None
                expr = (buf().map_range(begin, end))
                replace(begin, end,
                        '\n'.join(buf()[begin : end]) +
                        "\u00a0= " + value + "\u00a0")
                renderer.get_buffer().value = (expr,
                                               ((expr[1][0], expr[1][1] + 1),
                                                buf().map(end)))
                yield True

            if ty in ['expr', 'makecall']:
                replace(begin, end, value)
                renderer.get_buffer().call = None
                renderer.get_buffer().value = None
            elif ty == 'reset':
                # replace with original code
                chunk = original_code[begin[0] - 1 : end[0]].copy()
                chunk[-1] = chunk[-1][:end[1]]
                chunk[0] = chunk[0][begin[1] - 1:]
                replace(begin, end, '\n'.join(chunk))
                renderer.get_buffer().highlight = None
                renderer.get_buffer().value = None
                renderer.get_buffer().call = None
                continue
            elif ty == 'call':
                renderer.get_buffer().call = buf().map_range(begin, end)
                args = value.split(' ')
                b = args[0]
                e = args[1]
                name = ' ' + args[2] if len(args) > 2 else ''
                (b, e) = parse_slice(b, e)
                chunk = original_code[b[0] - 1 : e[0]].copy()
                chunk[-1] = chunk[-1][:e[1]]
                indentation = count_leading_spaces(chunk[0])
                chunk[0] = 'function' + name + chunk[0][b[1] - 1:]
                chunk[0] = ' ' * indentation + chunk[0]
                renderer.add_buffer(TextBuffer(chunk))
                buf().add_mapping((1, 0), (b[0], b[1] - 1),
                                  (1, 8 + len(name) + indentation))
                continue
            elif ty == 'showcall':
                renderer.get_buffer().call = buf().map_range(begin, end)
                continue
            elif ty == 'return':
                renderer.pop_buffer()
                b = renderer.get_buffer()

                if b is None:
                    renderer.del_coroutine(renderer.current_coroutine)
                else:
                    renderer.get_buffer().call = None
                continue
            elif ty == 'stmt':
                renderer.get_buffer().call = None
                renderer.get_buffer().highlight = (begin, end)
            elif ty == 'newco':
                args = value.split(' ')
                co_num = int(args[0])
                name = ' ' + args[1] if len(args) > 1 else ''
                chunk = original_code[begin[0] - 1 : end[0]].copy()
                chunk[-1] = chunk[-1][:end[1]]
                indentation = count_leading_spaces(chunk[0])
                chunk[0] = 'function' + name + chunk[0][begin[1] - 1:]
                chunk[0] = ' ' * indentation + chunk[0]
                current_coroutine = renderer.current_coroutine
                renderer.add_coroutine(co_num)
                renderer.add_buffer(TextBuffer(chunk))
                buf().add_mapping((1, 0), (begin[0], begin[1] - 1),
                                  (1, 8 + len(name) + indentation))
                renderer.set_coroutine(current_coroutine)
                continue
            elif ty == 'co':
                co_num = int(value)
                renderer.set_coroutine(co_num)
                continue
            else:
                raise ValueError('unknown event type: {}'.format(ty))
            yield True

    frame = 1
    print('Frame #{}'.format(frame))

    for _ in updater():
        if output != 'debug':
            output_surface = renderer.render()

            if output == 'display':
                surface.fill(Color('#282828'))
                surface.blit(output_surface, (0, 0))
                pygame.display.flip()
            elif output == 'file':
                save_image(output_surface,
                           os.path.join(sys.argv[3],
                                        'frame-{:05d}.png'.format(frame)))
            else:
                raise NotImplementedError

        if frame % 200 == 0:
            gc.collect()

        if len(sys.argv) < 5:
            input()
        elif sys.argv[4] != "0":
            time.sleep(float(sys.argv[3]))

        frame += 1
        print('Frame #{}'.format(frame))
