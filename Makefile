INTERPRETER_DIRECTORY = $(CURDIR)/lua
VISUALIZER_DIRECTORY = $(CURDIR)/python
EXAMPLE_DIRECTORY = $(CURDIR)/programs
OUTPUT_DIRECTORY = $(CURDIR)
FPS = 4

LUA = lua5.3 -e "package.path = package.path .. ';$(INTERPRETER_DIRECTORY)/?.lua'"
PYTHON = $(VISUALIZER_DIRECTORY)/env/bin/python3 -O

EXAMPLES = $(shell echo $(EXAMPLE_DIRECTORY)/*)
OUTDIRS = $(EXAMPLES:$(EXAMPLE_DIRECTORY)/%=$(OUTPUT_DIRECTORY)/%)
VIDEOS = $(EXAMPLES:$(EXAMPLE_DIRECTORY)/%=$(OUTPUT_DIRECTORY)/%/render.mp4)
DEBUG = $(EXAMPLES:$(EXAMPLE_DIRECTORY)/%=$(OUTPUT_DIRECTORY)/%/debug)

.PHONY: all debug clean

all: $(VIDEOS)

debug: $(DEBUG)
	@echo INTERPRETER_DIRECTORY=$(INTERPRETER_DIRECTORY)
	@echo VISUALIZER_DIRECTORY=$(VISUALIZER_DIRECTORY)
	@echo EXAMPLE_DIRECTORY=$(EXAMPLE_DIRECTORY)
	@echo OUTPUT_DIRECTORY=$(OUTPUT_DIRECTORY)
	@echo
	@echo LUA=$(LUA)
	@echo PYTHON=$(PYTHON)
	@echo
	@echo OUTDIRS=$(OUTDIRS)
	@echo EXAMPLES=$(EXAMPLES)
	@echo VIDEOS=$(VIDEOS)

clean:
	-rm -rf $(OUTDIRS)

$(OUTPUT_DIRECTORY)/%/render.mp4: $(OUTPUT_DIRECTORY)/%/frames
	DIMENSIONS=$$(for FILE in $(dir $<)rendered/*; do identify $$FILE; done | awk 'BEGIN { w = 0; h = 0 }; { split($$3, d, "x") }; d[1] > w { w = d[1] }; d[2] > h { h = d[2] }; \
		w % 2 == 1 { w += 1 }; h % 2 == 1 { h += 1 }; END { printf "w=%d:h=%d", w, h }') && \
		ffmpeg -nostdin -y -framerate $(FPS) -i "$(dir $<)rendered/frame-%05d.png" -c:v libx264 -crf 20 -profile:v main -preset slower -vf "pad=$${DIMENSIONS}:color=0x282828,format=yuv420p" "$@"

$(OUTPUT_DIRECTORY)/%/frames: $(VISUALIZER_DIRECTORY)/visual.py $(EXAMPLE_DIRECTORY)/%/program.lua $(OUTPUT_DIRECTORY)/%/events.txt
	-rm -rf "$(dir $@)rendered"
	mkdir -p "$(dir $@)rendered"
	$(PYTHON) $^ "$(dir $@)rendered" 0
	touch "$@"

$(OUTPUT_DIRECTORY)/%/debug: $(VISUALIZER_DIRECTORY)/visual.py $(EXAMPLE_DIRECTORY)/%/program.lua $(OUTPUT_DIRECTORY)/%/events.txt
	mkdir -p "$(dir $@)rendered"
	$(PYTHON) $^ debug 0

$(OUTPUT_DIRECTORY)/%/events.txt: $(EXAMPLE_DIRECTORY)/%/program.lua $(INTERPRETER_DIRECTORY)/interpret.lua
	mkdir -p "$(dir $@)"
	$(LUA) "$(INTERPRETER_DIRECTORY)/interpret.lua" "$<" "$@"
